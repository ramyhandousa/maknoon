<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::group([
    'namespace' => 'Api',
    'prefix' => 'v1'
], function () {

    Route::group(['prefix' => 'Auth'], function () {

        Route::post('login','AuthController@login');
        Route::post('sign_up_user', 'AuthController@register');
        Route::post('sign_up_doctor', 'AuthController@register_doctor');
        Route::post('active_user', 'AuthController@active_user');
        Route::post('accepted_user', 'AuthController@accepted_user');
        Route::post('changPassword', 'AuthController@changPassword');
        Route::post('edit_user_profile', 'AuthController@edit_user_profile');
        Route::post('edit_doctor_profile', 'AuthController@edit_doctor_profile');
        Route::post('upload_image','AuthController@upload');
        Route::post('remove_image','AuthController@removeImage');
        Route::post('remove_user','AuthController@removeUser');
        Route::post('remove_image_user','AuthController@removeUserImage');
        Route::post('check_data','AuthController@check_data');
    });

    Route::get('/users/{id}','UserController@show');
    Route::post('/users/payment','UserController@payment_doctor');
    Route::post('/users/test_payment','UserController@test_payment');

    Route::resource('children','ChildrenController');
    Route::post("children/check_have_children","ChildrenController@check_have_children");

    Route::get('home','HomeController@list_doctors');
    Route::get('list_doctors_drawing','HomeController@list_doctors_drawing');
    Route::get('show_doctor/{id}','HomeController@show_doctor');

    Route::group(['prefix' => 'lists'], function () {

        Route::get('categories','ListController@categories');
        Route::get('cities','ListController@cities');
        Route::get('questions','ListController@questions');

    });

    Route::resource('orders','OrderController');
    Route::post('orders/pay_order/{order}','OrderController@pay_order');
    Route::post('orders/refuse_order/{order}','OrderController@refuse_order');
    Route::post('orders/accepted_order/{order}','OrderController@accepted_order');
    Route::post('orders/finish_order/{order}','OrderController@finish_order');
    Route::post('orders/rate_order/{order}','OrderController@rate_order');


    Route::resource('conversation','MessageController');

    Route::resource('notifications','NotificationController');

    Route::group(['prefix' => 'setting'], function () {

        Route::get('reservation','SettingController@reservation_setting');
        Route::get('aboutUs','SettingController@aboutUs');
        Route::get('terms','SettingController@terms_user');
        Route::get('terms_doctor','SettingController@terms_doctor');
        Route::get('typesSupport','SettingController@getTypesSupport');
        Route::post('contact_us','SettingController@contact_us');
    });

    Route::post('test_notification','SettingController@testingNotification');

    Route::post('update-time-out/{order}','SettingController@updateTimeOut');

    Route::get("update_most_order",function (){

        $orders = \App\Models\Order::all();

        foreach ($orders as $order){
            $order->update(['status' => "accepted"]);
        }
    });
});
