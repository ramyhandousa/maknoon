<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    /**
     * @@ Home Dashboard
     */


    'dashboard' => 'لوحة التحكم',
    'settings' => 'الإعدادات',
    'profile' => 'الصفحة الشخصية',
    'logout' => 'تسجيل الخروج',
    'companies_management' => 'إدارة المنشأت',


    /**
     * @@ Countries Translations
     */


    'country_name' => 'اسم البلد',
    'countries' => 'البلاد المنشأة',


    /**
     * @@ Validation Messages
     */



    "suspendBecause" => " لقد تم تعطيل حسابك من قبل الإدارة لسبب :reason الرجاء التواصل مع الإدارة على الرقم :phone",
    'required' => ' :attribute  هذا الحقل إلزامي',
    'unique_phone' => 'هذا الجوال مستخدم من قبل',
    'digits_phone' =>  'الهاتف يجب ان يكون 10 ارقام فقط',
    'unique_username' => 'اسم المستخدم مستخدم من قبل',
    'unique_email' => 'البريد الإلكتروني مستخدم من قبل',
    'some_errors_happen' => 'حدثت بعد الأخطاء التالية',
    'field_required' => 'هذا الحقل مطلوب ادخاله',
    'password_not_confirmed' => 'كلمتي المرور غير متطابقة',
    'your_account_was_activated' => 'تم تفعيل حسابك بنجاح .',
    'activation_code_not_correct' => 'كود التفعيل غير صحيح .',
    'your_account_was_activated_before' => 'تم تفعيل حسابك من قبل',
    'activation_code_sent' => 'تم ارسال كود التفعيل الى جوالك بنجاح .',
    'activation_code_sent_email' => 'تم ارسال كود التفعيل الى إيميلك بنجاح .',
    'account_not_found' => 'لا يوجد حساب بهذا الرقم .',
    'your_account_not_activated_yet' => 'لم تقم بتفعيل حسابك بعد ',
    'your_account_suspended' => 'لقد تم حظر هذا الحساب',
    'phone_number_incorrect' => 'رقم جوالك القديم غير موجود لدينا .',
    'phone_number_notFound' => 'رقم جوالك  غير موجود لدينا .',
    'phone_edit_success' => 'تم تغير رقم الجوال بنجاح .',
    'profile_edit_success' => 'تم تعديل البيانات بنجاح .',
    'profile_not_edit_success' => 'لم يتم تعديل اي بيانات خاصه بك .',
    'logged_in_successfully' => 'تم تسجيل دخولك بنجاح .',
    'register_in_successfully' => 'تم الإشتراك بنجاح .',
    'logged_out_successfully' => 'تم تسجيل خروجك بنجاح .',
    'username_password_notcorrect' => 'البيانات غير صحيحة يرجي التأكد من فضلك',
    'user_with_roles' =>'هذا المستخدم ليس له صلاحيات للدخول',
    'passsord_reset_successfully' => 'تم استعادة كلمة المرور الخاصة بك بنجاح .',
    'reset_code_invalid' => ' كود التفعيل غير صحيح',
    'password_was_edited_successfully' => 'تم تعديل كلمة المرور بنجاح',
    'password_not_edited' => 'لم  يتعديل كلمة المرور ',
    'old_password_is_incorrect' => 'كلمة المرور القديمة غير صحيحة .',
    'order_was_added_successfully' => 'تم اضافة الطلب بنجاح .',
    'you_turned_on_notification' => 'لقد قمت بتفعيل الاشعارات بنجاح .',
    'you_turned_off_notification' => 'لقد قمت بالغاء تفعيل الاشعارات بنجاح .',
    'updated_successfully' => 'تم التعديل بنجاح .',
    'message_was_sent_successfully' => 'تم ارسال الرسالة  بنجاح .',
    'order_not_completed' => "طلبك غير مكتمل",
    'you_order_no' => "طلبك رقم ",
    'not_completed' => "غير مكتمل ",
    'order_search_other_city' => "جاري البحث في مدينة أخرى",
    'order_unavailable' => "طلبك غير متوفر",
    'order_priced' => "لقد تم تسعير طلبك",
    'is_unavailable' => "غير متوفر",

    'user_blocked_from_site_and_app' => 'سيتم حظر المستخدم عن الموقع والتطبيق',
    'user_unblocked_from_site_and_app' => 'سيتم إلغاء الحظر عن المستخدم فى الموقع والتطبيق',
    'block' => "حظر المستخدم",
    "unblock" => "إلغاء الحظر",
    "unblocked_success" => "لقد تم فك الحظر بنجاح",
    "blocked_success" => "لقد تم الحظر بنجاح",
    "visitor" => "زائر",
    "commerce_pieces_types" => "قطع الغيار التجارية",
    "piece_type" => "نوع القطعة",

    "battery_sizes" => "أحجام البطاريات",
    "battery_size" => "حجم البطارية",

    "jant_size" => "مقاس الجنط",
    "cover_size" => "مقاس الكفر",
    "add_new" => "إضافة جديد",

    'order_add_successfully' => 'تم إضافة طلبك بنجاح',

    'user_not_found' =>'هذا المستخدم غير موجود',
    'user_is_suspended' =>'هذا المستخدم محظور',
    'provider_not_found' =>'  مزود الخدمة غير موجود',
    'provider_rate_before' =>'  مزود الخدمة تم تقيمه من قبل',
    'rate_success' =>' تم التقيم بنجاح',
    'user_add_favorite' => 'تم اضافته ضمن مفضلتي',

    'user_remove_favorite' => 'تم  حذفه من ضمن مفضلتي',
    'member_ship_choose' => '  تم اختيار العضوية بنجاح يرجي الانتظار إلي ان يتم الموفقة عليها',
    'member_ship_register' => ' تم تسجيل العضوية بنجاح',
    'member_ship_not_found' => 'العضوية غير موجودة ',
    'city_not_found' => 'المدينة غير موجودة ',
    'product_not_found' => 'هذا المنتج غير موجود',
    'product_added' => 'تم إضافة المنتج بنجاح',
    'product_delete' => 'تم مسح المنتج بنجاح',
    'product_edit' => 'تم تعديل المنتج بنجاح',
    'product_count_error' => 'لا يمكنك إضافة منتج جديد نظرا للعضوية التي تمتلكها',
    'category_not_found' =>  'القسم غير موجودة ',
    'category_not_belong' =>  'القسم غير موجود لديك ',
    'category_add_success' =>  'تم إضافة القسم بنجاح ',
    'category_delete_success' =>  'تم مسح القسم بنجاح ',
    'category_found_before' =>  'هذا القسم تم إضافته من قبل ',
    'valid_memberShip_other' => 'لا يمكنك تغير العضوية حتي يتم الانتهاء من عضويتك',
    'offer_not_found' => 'هذا العرض غير موجود',
    'offer_found_before' => 'هذا العرض علي المنتج موجود سابقا يجب الانتهاء منه اولا',
    'offer_add' => 'تم إضافة عرض علي المنتج ',
    'offer_edit' => 'تم تعديل العرض علي المنتج ',
    'offer_delete' => 'تم مسح العرض علي المنتج ',
    'offer_price_error' => 'سعر العرض اكبر من تكلفة المنتج',
    'offer_date' => 'تاريخ بداية العرض يجب ان يكون نفس السنة و الشهر و اليوم اوكبر ',
    'offer_date_end' => 'تاريخ نهاية  العرض يجب ان يكون اكبر من تاريخ البداية ',
    'date_equals_today_or_max' => 'تاريخ الطلب لابد ان يكون اليوم او اكبر',
    'order_not_found' => 'رقم الطلب غير متوفر',
    'order_accpted' => 'تم الموافقة علي الطلب',
    'order_refuse' => 'تم رفض الطلب',
    'order_finish' => ' تم إنهاء الطلب بنجاح',
    'waiting' => 'قيد الانتظار',
    'accepted' => 'مقبول',
    'refuse' => 'مرفوض',
    'finish' => 'تم الانتهاء',
    'connect_us' =>  ' تواصل معنا ',
    'order_joining' =>  ' طلب انضمام   ',
    'join_as_service_provider' =>  '  يرغب في الانضمام كمزود خدمة  ',
    'products' =>  '  المنتجات  ',
    'add_product_name' =>  '  تم إضافة منتج جديد باسم ',
    'bank_transfers' =>  ' التحويلات البنكية ',
    'bank_transfers_from' =>  ' يوجد تحويل بنكي من ',
    'bank_transfers_accepted' =>  'تم قبول التحويل البنكي',
    'bank_transfers_refuse' =>  'تم قبول التحويل البنكي',
    'membership' =>  'العضويات',
    'membership_accepted' =>  'تم قبول عضويتك بنجاح ',
    'membership_refuse' =>  'تم رفض عضويتك ',
    'orders' =>  'الطلبات',
    'you_have_order' =>   ' يوجد طلب جديد لديك من  ' ,
    'you_order_accepted_on' =>   ' تم قبول طلبك علي الطلب رقم ' ,
    'you_order_refuse_on' =>    ' تم رفض طلبك علي الطلب رقم ' ,
    'rating' =>    ' التقيمات ' ,
    'you_rate_from' =>    ' تم تقيمك من  ' ,
    'offers' =>    ' العروض ' ,
    'offer_added_from' =>    ' تم إضافة عرض جديد من  ',
    'meal_not_found' =>    'هذه الوجبة غير متوفرة  ',
    'meal_edit' =>    ' تم تعديل الوجبة بنجاح',
    'meal_delete' =>    ' تم مسح الوجبة بنجاح',
    'know_app_message' =>    'من فضلك اكتب توضيحك ',


    'message_order_project' => 'تم إرسال طلبك بنجاح لمزودي الخدمات',
    'pending_order' => 'طلب جديد',
    'preparing_order' => 'جاري الطلب',
    'edit_order' => 'تم  تعديل علي الطلب بنجاح ',
    'accepted_order' => 'تم  الموافقة علي الطلب  ',
    'refuse_order' => 'تم رفض الطلب   بنجاح ',
    'delete_order' => 'تم مسح الطلب   بنجاح ',
    'refuse_user' => 'تم رفض الطلب من قبل العميل   ',
    'refuse_dealer' => 'تم رفض الطلب من قبل التاجر   ',
    'finish_order' => 'تم إنتهاء الطلب',
    'list_offer_empty' => 'تم إنتهاء الطلب لعدم وجود مزودي خدمات لتقديم أسعار',
    'success_progress' => 'تم العميلة بنجاح',
    'delivery_order' => 'تم تسليم الطلب بنجاح',

    'new_order' => ' لديك طلب جديد من ',
    'new_order_draw' => ' لديك طلب تحليل بالرسومات جديد من ',
    'order_accepted_by' => ' تم الموافقة علي الطلب من قبل ',
    'order_payed_by' => ' تم دفع الطلب بنجاح   ',
    'order_finish_by' => ' تم إنتهاء الطلب من قبل ',
    'order_refuse_by' => ' تم رفض الطلب من قبل ',
    'order_rate_by' => ' تم تقييم الطلب من قبل ',
    'order_not_refuse' => "لأسف لا يمكنك رفض الطلب لان تم تغير حالته",
    'order_not_accepted' => "لأسف لا يمكنك قبول الطلب لان تم تغير حالته",
    'order_not_finish' => "لأسف لا يمكنك إنتهاء الطلب لانه غير مقبول",
    'order_date_finish' => "لأسف لا يمكنك قبول الطلب لان تاريخ منتهي",

    'appointment' => 'حجز موعد' ,
    'draw' => 'تحليل بالرسومات',

    'not_have_children' =>  'للأسف لا يوجد أطفال حاليا لديك لإرسال طلبك',
    "finish_time_server" => "تم انتهاء معاد طلبك بسبب مرور الوقت المحدد ",
    "chatting" => "الشات",
    "finish_time_chatting_server" => "تم انتهاء الشات حسب الوقت المحدد من الإدارة ",

    "user_suspend_order"                => "يبدو ان هذا الأخصائي تم حظره او يوجد مشكلة في حسابه .",
    "user_order_date_unavailable"       => " نعتذر ولكن هذا الأخصائي مغلق في هذا اليوم.. ",
    "user_order_time_unavailable"       => " نعتذر ولكن هذا الأخصائي خارج التوقيت في هذا اليوم.. ",
    "user_order_children_unavailable"   => " للأسف لا يوجد أطفال حاليا لديك لإرسال طلبك  ",
    "user_order_child_unavailable"      => " تأكد من أنك لديك هذا الطفل  ",
    "user_order_category_unavailable"   => "تأكد من أن هذا الأخصائي لديه هذا القسم  ",
    "user_order_draw_unavailable"       => "يبدو ان هذا الأخصائي غير مشترك في التحليل بالرسومات.  ",


];
