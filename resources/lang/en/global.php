<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    /**
     * @@ Home Dashboard
     */


    'dashboard' => 'Dashboard',
    'settings' => 'Settings',
    'profile' => 'الصفحة الشخصية',
    'logout' => 'تسجيل الخروج',


    /**
     * @@ Countries Translations
     */


    "suspendBecause" => " You suspend Because :reason contact us on :phone",
    'country_name' => 'اسم البلد',
    'countries' => 'البلاد المنشأة',
    'field_required'=> 'This field is required .',
    'required'             => 'The :attribute field is required.',
    'unique_username' => 'This username is used  .',
    'unique_phone' => 'This phone number is used .',
    'digits_phone' => 'This phone number max number 10 .',
    'unique_email' => 'This Email is used .',
    'password_not_confirmed'=> 'Password is not confirmed',
    'some_errors_happen'=> 'Some errors happen',
    'your_account_was_activated'=> 'Your account was activated successfully .',
    'activation_code_not_correct'=> 'Activation code is not correct',
    'your_account_was_activated_before'=> 'Your account was activated before',
    'activation_code_sent'=>'Activation code has been sent successfully .',
    'activation_code_sent_email'=>'Activation code has been sent To Email successfully .',
    'account_not_found'=>'Account not found',
    'your_account_not_activated_yet'=> 'your account is not activated yet .',
    'your_account_suspended' => 'This account has been suspended',
    'phone_number_incorrect'=> 'Old Phone number is not correct .',
    'phone_number_notFound' => 'Phone number is not correct.',
    'phone_edit_success' => 'Phone Edit  Success .',
    'profile_edit_success' => 'Profile  Edit  successfully .',
    'profile_not_edit_success' => 'Profile Not have any Change',
    'logged_in_successfully'=> 'You are logged in successfully .',
    'register_in_successfully' => 'You are Register in successfully .',
    'logged_out_successfully' => 'logged out  successfully .',
    'username_password_notcorrect'=> 'Username or password is not correct .',
    'user_with_roles' =>'User Does`nt Have roles To login',
    'passsord_reset_successfully'=> 'you reset your password successfully .',
    'reset_code_invalid'=> 'Reset code is invalid',
    'password_was_edited_successfully'=>'Password was edited successfully .',
    'password_not_edited' => 'Password Not edited   ',
    'old_password_is_incorrect'=>'old password is not correct .',
    'order_was_added_successfully'=> 'Order was added successfully .',
    'you_turned_on_notification'=>'You turned on notifications .',
    'you_turned_off_notification'=>'You turned off notifications .',
    'updated_successfully'=> 'Updated successfully',
    'message_was_sent_successfully'=> 'Message was sent successfully .',
    'order_not_completed'=> "Your order not completed",
    'you_order_no'=> "Your order no ",
    'not_completed'=> "Not completed ",
    'order_search_other_city'=> " searching in other city",
    'order_unavailable' => "Order unavailable",
    'order_priced' => "Order Priced",
    'is_unavailable' => "unavailable",
    'user_blocked_from_site_and_app' => 'The user will be blocked from the site and app',
    'user_unblocked_from_site_and_app' => 'The user will be unblock from the site and app',
    'block' => "Block user",
    "unblock"  => "Unblock user",
    "unblocked_success" => "The user has been unblocked Successfully.",
    "blocked_success" => "The user has been blocked Successfully.",
    "visitor" => "Visitor",
    "commerce_pieces_types" => "Commercial Spar parts",
    "piece_type" => "Piece Type",


    "battery_sizes" => "Batteries Sizes",
    "battery_size" => "Battery Size",
    "jant_size" => "Jant Size",
    "cover_size" => "Cover Size",
    "add_new" => "Add New",

    'order_add_successfully' => 'Order Add Successfully',

    'user_not_found' =>'User  Not Found',
    'user_is_suspended' =>'User Is Suspended',
    'provider_not_found' => 'Provider  Not Found',
    'provider_rate_before' =>'  Provider was  rate Before',
    'rate_success' => 'Rate Successfully',
    'user_add_favorite' => 'added To Favorites Successfully',
    'user_remove_favorite' => 'removed From Favorites Successfully',
    'member_ship_choose' => 'Your Membership has been successfully.... waiting for admin accepted',
    'member_ship_register' => 'Your Membership has been successfully registered ',
    'member_ship_not_found' => 'Member Ship Not_found ',
    'city_not_found' => 'city not found ',

    'product_not_found' => 'Product Not Found',

    'product_added' => 'Product added Successfully',
    'product_edit' => 'Product edited Successfully',
    'product_count_error' => 'You can not add a new product because of the membership you own',
    'product_delete' => 'Product deleted Successfully',
    'category_not_found' => 'Category Not Found',
    'category_not_belong' => 'You Don`t have this Category',
    'category_add_success' =>  '  Category added Successfully',
    'category_delete_success' =>  'Category Delete Successfully ',
    'category_found_before' =>  'Category found Before ',

    'valid_memberShip_other' => 'You can`t choose another membership after finish your memberShip',
    'offer_not_found' =>'Offer Not Found',
    'offer_found_before' => 'Offer Found Before on this product',
    'offer_add' =>'Offer added Successfully',
    'offer_edit' => 'Offer edited Successfully ',
    'offer_delete' => 'Offer deleted Successfully',
    'offer_price_error' => 'Price Offer Bigger Than Price Product',
    'offer_date' => 'Must be the same year and same month and the same day or old',
    'offer_date_end' =>'The end date of the offer must be greater than Start date',
    'date_equals_today_or_max' => 'date order must Bigger than Today Or equal Today',
    'order_not_found' => 'order Id not found',
    'order_accpted' => 'Order accpted Successfully',
    'order_refuse' =>  'Order refuse Successfully',
    'order_finish' => 'Order Finish Successfully',
    'waiting' => 'waiting',
    'accepted' => 'accepted',
    'refuse' => 'refuse',
    'finish' => ' finish',
    'connect_us' =>  ' Contact Us   ',
    'order_joining' =>  ' Application for admission  ',
    'join_as_service_provider' =>  ' Allows to join as a service provider ',
    'products' =>  '  products  ',
    'add_product_name' =>  '  A new product has been added as  ',
    'bank_transfers' =>  '  Bank transfers ',
    'bank_transfers_from' =>  '  There is a bank transfer from ',
    'bank_transfers_accepted' =>  'bank transfers accepted',
    'bank_transfers_refuse' =>  'bank transfers refuse',
    'membership' =>  'memberShip',
    'membership_accepted' =>  'membership accepted Successfully',
    'membership_refuse' =>  ' membership refuse ',
    'orders' =>  'orders',
    'you_have_order' =>   ' You have order from ' ,
    'you_order_accepted_on' =>   ' you order accepted on ' ,
    'you_order_refuse_on' =>   ' you order refuse on ' ,
    'rating' =>    ' rating ' ,
    'you_rate_from' =>    ' you rate from  ' ,
    'offers' =>    ' offers ' ,
    'offer_added_from' =>    ' Offer added From ',
    'meal_not_found' =>    'Meal Not Found',
    'meal_edit' =>    'Meal Edit Successfully',
    'meal_delete' =>    'Meal Delete Successfully',
    'know_app_message' =>    'Please type more clarification',


    'message_order_project' => 'Your Order Send Successfully To Providers',

    'pending_order' => 'Pending Order' ,
    'edit_order' => 'Edit Order Successfully',
    'preparing_order' =>  'Preparing Order' ,
    'accepted_order' => 'Accepted Order',
    'refuse_order' => 'Refuse Order Successfully' ,
    'delete_order' => 'Delete Order Successfully',
    'refuse_user' => 'Refuse Order by User' ,
    'refuse_dealer' => 'Refuse Order by Dealer' ,
    'finish_order' =>  'Finish Order',
    'success_progress' => 'Success Progress',
    'delivery_order' => 'Delivery Order Success',


    'new_order' => ' New Order From ' ,
    'new_order_draw' => ' You have a new graphic analysis request from ',


    'order_accepted_by' => ' Your Order Accepted By ',
    'order_payed_by' => ' Your Order Paid Success ',
    'order_finish_by' => ' Your Order Finish By ',
    'order_refuse_by' => ' Your Order reject By ',
    'order_rate_by' => ' Your Order Rate By',

    'order_not_refuse' => "Unfortunately, you cannot reject the request because its status has changed",
    'order_not_accepted' => "Unfortunately, you cannot accepted the request because its status has changed",
    'order_not_finish' => "Unfortunately, you cannot finish the request because its status has changed",
    'order_date_finish' => "Unfortunately, you cannot accept the application because the date has expired",

    'appointment' => 'Appointment Booking' ,
    'draw' => 'Graphic analysis',

    "not_have_children" => "Unfortunately there are no children currently you have to send your request",

    "finish_time_server" => " Your re-order has expired because the specified time has passed ",

    "chatting" => "chatting",
    "finish_time_chatting_server" => "The chat has been terminated according to the time specified by the administration",

    "user_suspend_order"                => "It appears that this specialist has been blocked or has a problem with his account",
    "user_order_date_unavailable"       => "   We apologize, but this specialist is closed for this day. ",
    "user_order_time_unavailable"       => " We apologize, but this specialist is out of time on this day. ",
    "user_order_children_unavailable"   => " Unfortunately there are no children currently you have to send your request ",
    "user_order_child_unavailable"      => " Make sure you have this kid ",
    "user_order_category_unavailable"   => "Make sure this specialist has this department  ",
    "user_order_draw_unavailable"       => "It appears that this specialist is not involved in graphic analysis.  ",
];
