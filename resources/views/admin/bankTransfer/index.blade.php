@extends('admin.layouts.master')

@section('title', '{{$pageName}}')
@section('styles')

    <!-- Custom box css -->
    <link href="/assets/admin/plugins/custombox/dist/custombox.min.css" rel="stylesheet">

    <style>
        .errorValidationReason{

            border: 1px solid red;

        }
    </style>
@endsection
@section('content')


    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15 ">

            </div>
            <h4 class="page-title">{{$pageName}}</h4>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h4 class="header-title m-t-0 m-b-30">{{$pageName}}</h4>

                <table id="datatable-fixed-header" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th width="12%">صورة الإيصال</th>
                        <th>إسم الدعوة </th>
                        <th>إسم الداعي </th>
                        <th>إسم البنك </th>
                        <th>تم القبول او الرفض</th>
                        <th>@lang('trans.options')</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($banks as $row)
                        <tr>
                            <td>
                                @if($row->image)
                                    <a   data-fancybox="gallery"
                                         href="{{ $helper->getDefaultImage(URL('/').'/'. $row->image, request()->root().'/default.png') }}">
                                        <img class="img" style="width: 50px;height: 50px;object-fit: cover;border-radius: 10px;"
                                             src="{{ $helper->getDefaultImage( URL('/').'/'. $row->image, request()->root().'/default.png') }}"/>
                                    </a>
                                @else
                                    لا يوجد صورة
                                @endIf

                            </td>
                            <td>{{ optional($row->invitation)->name }}</td>
                            <td>{{ optional($row->user)->name }}</td>
                            <td> {{ $row->bank->name_ar }}  </td>
                            <td>
                                @if($row->is_accepted == 0)
                                    لم يتم حدوث اي عميلة
                                @elseif($row->is_accepted == 1 )
                                    <img width="23px" src="{{ asset('assets/admin/images/ok.png') }}" alt="">
                                @else
                                    <img  width="23px" src="{{ asset('assets/admin/images/false.png') }}" alt="">
                                @endif
                            </td>

                            <td>
                                @if($row->is_accepted == -1)
                                <a class="label label-default" data-toggle="modal" data-target="#con-close-modal{{$row->id}}">  مشاهدة سبب الرفض</a>
                                @endif
                                <div id="con-close-modal{{$row->id}}" class="modal fade" tabindex="-1"
                                     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">تفاصيل التحويلات</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-1" class="control-label">  سبب رفض التحويل </label>
                                                            <textarea class="form-control" readonly rows="3">{{ $row->message }} </textarea>                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if($row->is_accepted == 0 )
                                    <a href="javascript:;" data-toggle="modal" data-target="#con-close-modal2" data-id="{{$row->id}}" data-action="suspend"
                                       data-url="{{route('refuse-transfer-admin')}}"
                                       class="suspendWithReason label label-danger">رفض</a>


                                    <a id="elementRow{{$row->id}}" href="javascript:;" data-id="{{$row->id}}" data-action="activate"
                                       data-url="{{route('accept-transfer-admin')}}"
                                       class="suspendOrActivate label label-success">موافقة</a>
                                @endif



                                <form id="refuseForm" data-parsley-validate novalidate method="POST" action="{{route('refuse-transfer-admin')}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div id="con-close-modal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">ﺭﻓﺾ اﻟﻄﻠﺐ</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="hidden" id="idHolder" name="id">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">سبب الرفض</label>
                                                                <textarea  name="refuse_reason" data-parsley-required data-parsley-required-message="سبب الرفض مطلوب "
                                                                           maxlength="250"  class="form-control autogrow" id="refuseField"
                                                                           placeholder="سبب الرفض" style="min-width: 100% ;overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">ﺇﻟﻐﺎء</button>
                                                    <button type="submit" class="btn btn-danger waves-effect waves-light">رفض </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </form>

                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection


@section('scripts')


    <!-- Modal-Effect -->
    <script src="/assets/admin/plugins/custombox/dist/custombox.min.js"></script>
    <script src="/assets/admin/plugins/custombox/dist/legacy.min.js"></script>
    <script>



        $('body').on('click', '.suspendOrActivate', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());
            var action = $(this).attr('data-action');
            var text = '';
            var type = '';
            var confirmButtonClass = '';
            var redirectionRoute = '';

            text = "هل تريد قبول الطلب فعلا ؟";
            type = 'success';
            confirmButtonClass = 'btn-success waves-effect waves-light';
            redirectionRoute = '{{route('bank-transfer-admin')}}';


            swal({
                    title: "هل انت متأكد؟",
                    text: text,
                    type: type,
                    showCancelButton: true,
                    confirmButtonColor: "#27dd24",
                    confirmButtonText: "موافق",
                    cancelButtonText: "إلغاء",
                    confirmButtonClass:confirmButtonClass,
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                function (isConfirm) {
                    if(isConfirm){
                        $.ajax({
                            type:'post',
                            url :url,
                            data:{id:id},
                            dataType:'json',
                            success:function(data){
                                if(data.status == true){
                                    var title = data.title;
                                    var msg = data.message;
                                    toastr.options = {
                                        positionClass : 'toast-top-left',
                                        onclick:null
                                    };

                                    var $toast = toastr['success'](msg,title);
                                    $toastlast = $toast;

                                    function pageRedirect() {
                                        window.location.href =redirectionRoute;
                                    }
                                    setTimeout(pageRedirect(), 750);
                                }else {
                                    var title = data.title;
                                    var msg = data.message;
                                    toastr.options = {
                                        positionClass : 'toast-top-left',
                                        onclick:null
                                    };

                                    var $toast = toastr['error'](msg,title);
                                    $toastlast = $toast
                                }
                            }
                        });
                    }

                }
            );
        });



        $('body').on('click', '.suspendWithReason', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var idHolder = $('#idHolder').attr('value',id);

            $('#suspendForm').on('submit',function (e) {
                e.preventDefault();
                var suspendReason = $('#suspendField').val();

                $.ajax({
                    type:'post',
                    url :url,
                    data:{id:id,suspendReason:suspendReason},
                    dataType:'json',
                    success:function(data){
                        if(data.status == true){
                            var title = data.title;
                            var msg = data.message;
                            toastr.options = {
                                positionClass : 'toast-top-left',
                                onclick:null
                            };

                            var $toast = toastr['success'](msg,title);
                            $toastlast = $toast

                            function pageRedirect() {
                                window.location.href ='{{route('bank-transfer-admin')}}';
                            }
                            setTimeout(pageRedirect(), 750);
                        }else {
                            var title = data.title;
                            var msg = data.message;
                            toastr.options = {
                                positionClass : 'toast-top-left',
                                onclick:null
                            };

                            var $toast = toastr['error'](msg,title);
                            $toastlast = $toast
                        }
                    }
                });

            })

        });



    </script>


@endsection



