@extends('admin.layouts.master')

@section('title' , __('maincp.public_notication'))

@section('content')

    <form data-parsley-validate novalidate method="POST" action="{{ route('send.public.notifications') }}"
          enctype="multipart/form-data">
    {{ csrf_field() }}
    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">


                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>


                </div>
                <h4 class="page-title">@lang('maincp.public_notication')</h4>
            </div>



            <div class="col-lg-12   ">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('maincp.content_notify')</h4>


                    <div class="form-group">
                        <label for="userName">@lang('trans.title_notify_arabic') *</label>
                        <input type="text" name="title" parsley-trigger="change" required
                               placeholder="@lang('trans.title_notify_arabic')..." class="form-control requiredField "
                               id="userName">
                    </div>



                    <div class="form-group {{ $errors->has('notification_message') ? 'has-error' : '' }}">
                        <label for="notification_message">@lang('trans.content_notify_arabic') </label>
                        <textarea  class="form-control requiredField " name="message" required></textarea>
                    </div>


                    <div class="form-group text-right m-b-0 ">
                        <button class="btn btn-warning waves-effect waves-light m-t-20"
                                type="submit">@lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20"> @lang('maincp.disable')
                        </button>
                    </div>


                </div>
            </div><!-- end col -->

        </div>
        <!-- end row -->
    </form>


@endsection


@section('scripts')



@endsection



