@extends('admin.layouts.master')
@section('title', __('maincp.user_data'))
@section('content')


    <form method="POST" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}
    {{ method_field('PUT') }}



    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    {{--<button type="button" class="btn btn-custom  waves-effect waves-light"--}}
                        {{--onclick="window.history.back();return false;"> @lang('maincp.back') <span class="m-l-5"><i--}}
                                {{--class="fa fa-reply"></i></span>--}}
                    {{--</button>--}}

                    <a href="{{ route('users.index') }}"
                       class="btn btn-custom  waves-effect waves-light">
												<span><span>رجوع  </span>
													<i class="fa fa-reply"></i>
												</span>
                    </a>

                </div>
                <h4 class="page-title">@lang('maincp.user_data') </h4>
            </div>
        </div>


        <div class="row">


                <div class="col-sm-12">

                <div class="card-box">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card-box p-b-0">


                                <h4 class="header-title m-t-0 m-b-30">التفاصيل</h4>

                                <form>
                                    <div id="basicwizard" class=" pull-in">
                                        <ul class="nav nav-tabs navtab-wizard nav-justified bg-muted">
                                            <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">البيانات الاساسية</a></li>
                                            @if($user->profile)
                                                <li ><a href="#tab2" data-toggle="tab" aria-expanded="false"> الملف الشخصي  </a></li>
                                            @endif

                                                <li ><a href="#tab3" data-toggle="tab" aria-expanded="false"> التخصصات  </a></li>
                                                <li ><a href="#tab4" data-toggle="tab" aria-expanded="false"> أيام العمل الأسبوعي  </a></li>

                                        </ul>
                                        <div class="tab-content b-0 m-b-0">
                                            <div class="tab-pane m-t-10 fade active in" id="tab1">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="col-xs-12 col-lg-12">
                                                            <h4>@lang('maincp.personal_data')</h4>
                                                            <hr>
                                                        </div>

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>@lang('maincp.full_name') :</label>
                                                            <input class="form-control" value="{{ $user->name }}"><br>
                                                        </div>

                                                        @if($user->phone )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.mobile_number') :</label>
                                                                <input class="form-control" value="{{ $user->phone }}"><br>
                                                            </div>
                                                        @endif

                                                        @if($user->email )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.e_mail')  :</label>
                                                                <input class="form-control" value="{{ $user->email }}"><br>
                                                            </div>
                                                        @endif

                                                        @if($user->city )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>المدينة  :</label>
                                                                <input class="form-control" value="{{ optional($user->city)->name_ar }}"><br>
                                                            </div>
                                                        @endif

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>إشتراك في التحليل بالرسومات  :</label>
                                                            <input class="form-control" value="{{  $user->subscribe_draw ? 'نعم' : 'لا ' }}"><br>
                                                        </div>

                                                    </div>



                                                    <div class="col-sm-4">
                                                        <div class="card-box">
                                                            <div class="row">

                                                                <div class="card-box" style="overflow: hidden;">
                                                                    <h4 class="header-title m-t-0 m-b-30">@lang('institutioncp.personal_image')</h4>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage( $user->image?request()->root().'/'.$user->image :null, request()->root().'/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage( $user->image?request()->root().'/'.$user->image :null, request()->root().'/default.png') }}"/>
                                                                            </a>
                                                                        </div>

                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                            @if($user->profile)
                                                <div class="tab-pane m-t-10 fade  in" id="tab2">
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="col-xs-12 col-lg-12">
                                                                <h4>البيانات الفرعية</h4>
                                                                <hr>
                                                            </div>

                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label> العمر :</label>
                                                                <input class="form-control" value="{{ optional($user->profile)->age }}"><br>
                                                            </div>

                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>  الدرجة العلمية :</label>
                                                                <input class="form-control" value="{{ optional($user->profile)->degree }}"><br>
                                                            </div>

                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label> عدد سنين الخبرة :</label>
                                                                <input class="form-control" value="{{ optional($user->profile)->experience_year }}"><br>
                                                            </div>

                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label> الوصف الخاص به :</label>

                                                                <textarea class="form-control">
                                                                    {{ optional($user->profile)->desc }}
                                                                </textarea>
                                                            </div>
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>   الشهادات :</label>

                                                                @if(count(optional($user->profile)->certificates) > 0)
                                                                    <div class="form-group">
                                                                        @foreach(optional($user->profile)->certificates as $image)

                                                                            @if(pathinfo( $image, PATHINFO_EXTENSION) == 'pdf')
                                                                                <div class="col-sm-3">
                                                                                    <a data-fancybox="gallery"  href="{{  $helper->getDefaultImage(URL('/').'/'.$image , request()->root().'/pdf.jpg') }}">
                                                                                        <img style="width: 50%; height: 30%;"
                                                                                             src="{{ $helper->getDefaultImage( request()->root().'/pdf.jpg' , request()->root().'/pdf.jpg') }}"/>
                                                                                    </a>
                                                                                </div>

                                                                            @elseif(pathinfo( $image, PATHINFO_EXTENSION) == 'docx')
                                                                                <div class="col-sm-3">
                                                                                    <a data-fancybox="gallery"  href="{{  $helper->getDefaultImage(URL('/').'/'.$image , request()->root().'/Excel-Logo.jpg') }}">
                                                                                        <img style="width: 50%; height: 30%;"
                                                                                             src="{{ $helper->getDefaultImage( request()->root().'/Excel-Logo.jpg' , request()->root().'/Excel-Logo.jpg') }}"/>
                                                                                    </a>
                                                                                </div>

                                                                            @else
                                                                                <div class="col-sm-3">
                                                                                    <a   data-fancybox="gallery"
                                                                                         href="{{ $helper->getDefaultImage(URL('/').'/'. $image, request()->root().'/default.png') }}">
                                                                                        <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                             src="{{ $helper->getDefaultImage( URL('/').'/'. $image, request()->root().'/default.png') }}"/>
                                                                                    </a>
                                                                                </div>
                                                                            @endif
                                                                        @endforeach
                                                                    </div>
                                                                @else
                                                                    <div class="col-sm-12">
                                                                        لا يوجد صور
                                                                    </div>
                                                                @endif

                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            @endif

                                            <div class="tab-pane m-t-10 fade  in" id="tab3">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="col-xs-12 col-lg-12">
                                                            <h4>التخصصات الخاصة بالأخصائي</h4>
                                                            <hr>
                                                        </div>

                                                        @if(count( $user->categories) > 0)
                                                            @foreach($user->categories as $category)
                                                                <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">{{$category->name_ar}}</div>
                                                                    </div>
                                                                </div>

                                                            @endforeach
                                                        @else
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">ليس لديه اي تخصصات حاليا</div>
                                                                </div>
                                                            </div>
                                                        @endif


                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane m-t-10 fade  in" id="tab4">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="col-xs-12 col-lg-12">
                                                            <h4>أيام العمل  </h4>
                                                            <hr>
                                                        </div>

                                                        @if(count( $user->days) > 0)
                                                            @foreach($user->days as $day_work)
                                                                <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">{{$day_work->name_ar}}</div>

                                                                        @if($day_work->pivot->working)
                                                                            <div class="panel-body">
                                                                                 من الساعة   &nbsp;&nbsp;
                                                                                {{date('h:i A', strtotime($day_work->pivot->start))}}
                                                                                إلي  &nbsp;&nbsp;
                                                                                {{date('h:i A', strtotime($day_work->pivot->end))}}
                                                                            </div>
                                                                        @endif
                                                                        <div class="panel-footer">
                                                                            <div class="text-center">{{$day_work->pivot->working ? 'يعمل في هذا اليوم' : 'لا يعمل'}}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            @endforeach
                                                        @else
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">ليس لديه اي أيام عمل حاليا</div>
                                                                </div>
                                                            </div>
                                                        @endif


                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

        </div>


    </form>

@endsection

