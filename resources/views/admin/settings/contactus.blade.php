@extends('admin.layouts.master')

@section("title", __("maincp.call_us"))
@section('styles')

    <style>
        .customeStyleSocail{

            margin: 10px auto;

        }
    </style>
@endsection
@section('content')
    <form action="{{ route('administrator.settings.store') }}" data-parsley-validate="" novalidate="" method="post"
          enctype="multipart/form-data">
    {{ csrf_field() }}
    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-0">
                    <div class="btn-group pull-right m-t-15">
                        <button type="button" class="btn btn-custom  waves-effect waves-light"
                                onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                        class="fa fa-reply"></i></span>
                        </button>
                    </div>

                </div>
                <h4 class="page-title">إعدادات عامة للتطبيق </h4>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive m-t-0">

                    <div class="form-group">

{{--                        <div class="col-lg-5 col-xs-12">--}}
{{--                            <label>عمولة التطبيق  بالنسبة %</label>--}}
{{--                            <input class="form-control " type="number" style="margin: 15px auto" name="app_percentage"--}}
{{--                                   value="{{ $setting->getBody('app_percentage') }}" placeholder="العمولة"--}}
{{--                                   --}}{{--min="0" --}}
{{--                                   oninput="this.value = Math.abs(this.value)"--}}
{{--                                   --}}{{--data-parsley-min-message="    يجب أن تكون العمولة اكثر من  او يساوي 0 "--}}
{{--                                   maxlength="500" >--}}
{{--                        </div>--}}

{{--                        <div class="col-lg-5 col-xs-12">--}}
{{--                            <label>تحويل المبلغ يوم ؟  (  في الأسبوع ) </label>--}}

{{--                            <select class="form-control"  name="money_transfer">--}}
{{--                                <option value="Saturday"  @if( $setting->getBody('money_transfer')  == 'Saturday' )selected @endif>السبت</option>--}}
{{--                                <option value="Sunday"  @if( $setting->getBody('money_transfer')  == 'Sunday' )selected @endif>الأحد</option>--}}
{{--                                <option value="Monday"  @if( $setting->getBody('money_transfer')  == 'Monday' )selected @endif>الإثنين</option>--}}
{{--                                <option value="Tuesday"  @if( $setting->getBody('money_transfer')  == 'Tuesday' )selected @endif>الثلاثاء</option>--}}
{{--                                <option value="Wednesday" @if( $setting->getBody('money_transfer')  == 'Wednesday' )selected @endif>الأربعاء</option>--}}
{{--                                <option value="Thursday"  @if( $setting->getBody('money_transfer')  == 'Thursday' )selected @endif>الخميس</option>--}}
{{--                                <option  value="Friday" @if( $setting->getBody('money_transfer')  == 'Friday' )selected @endif >الجمعة</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}

                        {{--<div class="col-lg-5 col-xs-12">--}}
                            {{--<label>الدفع عند الاستلام </label>--}}
                            {{--<input class="form-control" type="number" name="payment_cash" min="0"--}}
                                   {{--value="{{ $setting->getBody('payment_cash') }}" placeholder="كاش"--}}
                                   {{--maxlength="500" ><br>--}}
                        {{--</div>--}}

                        {{--<div class="col-lg-10 col-xs-12">--}}
                            {{--<label>ضريبة القيمة المضافة: </label>--}}
                            {{--<input class="form-control" type="number" name="taxs" min="0"--}}
                                   {{--value="{{ $setting->getBody('taxs') }}" placeholder="كاش"--}}
                                   {{--maxlength="500" ><br>--}}
                        {{--</div>--}}

{{--                        <div class="col-lg-5 col-xs-12">--}}
{{--                            <label>@lang('maincp.unified_number') </label>--}}
{{--                            <input class="form-control" type="text" name="phone_contact"--}}
{{--                                   value="{{ $setting->getBody('phone_contact') }}" placeholder="0123456789"--}}
{{--                                   maxlength="500" >--}}
{{--                        </div>--}}

{{--                        <div class="col-lg-5 col-xs-12">--}}
{{--                            <label>@lang('maincp.e_mail') </label>--}}
{{--                            <input class="form-control" type="email" name="contactus_email"--}}
{{--                                   value="{{ $setting->getBody('contactus_email') }}" placeholder="Example@Advertisement.sa"--}}
{{--                                   maxlength="500"--}}
{{--                                   >--}}
{{--                        </div>--}}

                        <div class="col-lg-5 col-xs-12">
                            <div class="input-group customeStyleSocail">
                                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-facebook"></i></span>
                                <input type="text" class="form-control" name="faceBook"
                                       value="{{ $setting->getBody('faceBook') }}"
                                       placeholder="@lang('maincp.facebook') "
                                       aria-label="Recipient's username" aria- describedby="basic-addon2"
                                       maxlength="500" >
                            </div>
                        </div>

                        <div class="col-lg-5 col-xs-12">
                            <div class="input-group customeStyleSocail">
                                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-twitter"></i></span>
                                <input type="text" name="twitter"
                                       value="{{ $setting->getBody('twitter') }}" class="form-control"
                                       placeholder="@lang('maincp.twitter') "
                                       aria-label="Recipient's username" aria- describedby="basic-addon2"
                                       maxlength="500" >
                            </div>
                        </div>

{{--                        <div class="col-lg-5 col-xs-12">--}}
{{--                            <div class="input-group customeStyleSocail">--}}
{{--                                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-whatsapp" aria-hidden="true"></i></span>--}}
{{--                                <input type="text" name="whatsapp"--}}
{{--                                       value="{{ $setting->getBody('whatsapp') }}" class="form-control"--}}
{{--                                       placeholder=" "--}}
{{--                                       aria-label="Recipient's username" aria- describedby="basic-addon2"--}}
{{--                                       maxlength="500" >--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="col-lg-5 col-xs-12">--}}
{{--                            <div class="input-group customeStyleSocail">--}}
{{--                                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-telegram" aria-hidden="true"></i></span>--}}
{{--                                <input type="text" name="telegram"--}}
{{--                                       value="{{ $setting->getBody('telegram') }}" class="form-control"--}}
{{--                                       placeholder=" "--}}
{{--                                       aria-label="Recipient's username" aria- describedby="basic-addon2"--}}
{{--                                       maxlength="500" >--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        {{--<div class="col-lg-5 col-xs-12">--}}
                            {{--<div class="input-group">--}}
                                {{--<span class="input-group-addon" id="basic-addon2"><i class="fa fa-pinterest"></i></span>--}}
                                {{--<input type="text" name="contactus_pinterest"--}}
                                       {{--value="{{ $setting->getBody('contactus_pinterest') }}" class="form-control"--}}
                                       {{--placeholder=" "--}}
                                       {{--aria-label="Recipient's username" aria- describedby="basic-addon2"--}}
                                       {{--maxlength="500">--}}
                            {{--</div>--}}
                        {{--</div>--}}



                        <div class="col-lg-5 col-xs-12">
                            <div class="input-group customeStyleSocail">
                                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-snapchat"></i></span>
                                <input type="text" name="snapChat"
                                       value="{{ $setting->getBody('snapChat') }}" class="form-control"
                                       placeholder=" "
                                       aria-label="Recipient's username" aria- describedby="basic-addon2"
                                       maxlength="500">
                            </div>
                        </div>




                        <div class="col-lg-5 col-xs-12">
                            <div class="input-group customeStyleSocail">
                                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-instagram"></i></span>
                                <input type="text" name="instagram"
                                       value="{{ $setting->getBody('instagram') }}" class="form-control"
                                       placeholder="@lang('maincp.instagram')  "
                                       aria-label="Recipient's username" aria- describedby="basic-addon2"
                                       maxlength="500">
                            </div>
                        </div>

                        {{--<div class="col-lg-12 col-xs-12">--}}
                            {{--<div class="input-group customeStyleSocail">--}}
                                {{--<label>لينك فيديو التطبيق  </label>--}}
                                {{--<span class="input-group-addon" id="basic-addon2"> </span>--}}
                                {{--<input type="text" name="how-to-video"--}}
                                       {{--value="{{ $setting->getBody('how-to-video') }}" class="form-control"--}}
                                       {{--placeholder="لينك الفديو  "--}}
                                       {{--aria-label="Recipient's username" aria- describedby="basic-addon2"--}}
                                       {{--maxlength="500">--}}
                            {{--</div>--}}
                        {{--</div>--}}


                        <div class="col-xs-12 text-right">

                            <button type="submit" class="btn btn-warning">
                               @lang('maincp.save_data')   <i style="display: none;" id="spinnerDiv"
                                                class="fa fa-spinner fa-spin"></i>
                            </button>

                        </div>

                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </form>
@endsection


@section('scripts')
    <script type="text/javascript">

        $('form').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(this);


            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid()) {
                $('#spinnerDiv').show();
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {


                        if (data.status == true) {
                            //  $('#messageError').html(data.message);
                            $('#spinnerDiv').hide();
                            var shortCutFunction = 'success';
                            var msg = data.message;
                            var title = 'نجاح';
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                            $toastlast = $toast;
                        }

                        {{--setTimeout(function () {--}}
                        {{--window.location.href = '{{ route('categories.index') }}';--}}
                        {{--}, 3000);--}}
                    },
                    error: function (data) {
                    }
                });
            }
        });

    </script>
@endsection







