@extends('admin.layouts.master')
@section('title' ,__('maincp.about_us'))
@section('content')
    <form action="{{ route('administrator.settings.store') }}" data-parsley-validate="" novalidate="" method="post"
          enctype="multipart/form-data">

    {{ csrf_field() }}

    <!-- Page-Title -->

        <div class="row">
            <div class="col-sm-12 ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                class="fa fa-reply"></i></span>
                    </button>

                </div>
                <h4 class="page-title">الإعدادات العامة للتطبيق :  </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <div class="col-xs-6">
                        <div class="col-lg-10 col-xs-12">
                            <label for="minutes_control">الوقت المسموح به لدفع قيمة الحجز من قبل
                                المستخدم وبعد انقضاء المدة يتم إلغاء الطلب تلقائيا   </label>
                            <input type="number" min="2" name="minutes_control" oninput="validity.valid||(value='');"
                                   max="1000" required placeholder="أقل توقيت   يجب ان يكون 2 دقيقة"
                                   data-parsley-required-message="  التوقيت مطلوب"
                                   data-parsley-min-message="أقل توقيت   يجب ان يكون 2 دقيقة"
                                   data-parsley-maxlength-message=" أقصى عدد الحروف المسموح بها هى (1000) حرف"
                                   value="{{ $setting->getBody('minutes_control') }}"
                                   class="form-control " >
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="col-lg-10 col-xs-12">
                            <label for="close_chatting">الوقت المسموح به الشات لإغلاقه </label>
                            <input type="number" min="1" name="close_chatting" oninput="validity.valid||(value='');"
                                   max="1000" required placeholder="أقل توقيت   يجب ان يكون 2 دقيقة"
                                   data-parsley-required-message="  التوقيت مطلوب"
                                   data-parsley-min-message="أقل توقيت   يجب ان يكون 2 دقيقة"
                                   data-parsley-maxlength-message=" أقصى عدد الحروف المسموح بها هى (1000) حرف"
                                   value="{{ $setting->getBody('close_chatting') }}"
                                   class="form-control " >
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="col-lg-10 col-xs-12">
                            <label>  تحديد نسبة التطبيق علي الحجوزات الخاصة ( بحجز موعد ) </label>
                            <input class="form-control number" type="number" oninput="validity.valid||(value='');" style="margin: 15px auto" name="tax_book_appointment"
                                   value="{{ $setting->getBody('tax_book_appointment') }}" placeholder="تحديد نسبة التطبيق علي الحجوزات الخاصة ( بحجز موعد )"
                                   maxlength="500" >
                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="col-lg-10 col-xs-12">
                            <label>نسبة التطبيق علي الحجوزات الخاصة  ( تحليل رسومات ) </label>
                            <input class="form-control number" type="number" oninput="validity.valid||(value='');" style="margin: 15px auto" name="tax_book_draw"
                                   value="{{ $setting->getBody('tax_book_draw') }}" placeholder="نسبة التطبيق علي الحجوزات الخاصة  ( تحليل رسومات )"
                                   maxlength="500" >
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="col-lg-10 col-xs-12">
                            <label>  ( قيمة ) سعر ( خدمة التحليل بالرسومات ) مبلغ بالريال
                                 </label>
                            <input class="form-control number" type="number" oninput="validity.valid||(value='');"
                                   style="margin: 15px auto" name="value_book_draw"
                                   value="{{ $setting->getBody('value_book_draw') }}" placeholder="سعر ( خدمة التحليل بالرسومات )"
                                   maxlength="500" >
                        </div>
                    </div>

                    <hr>
                    <div class="col-xs-12">
                        <h4>اضافة مدة وقيمة حجز الإستشارات الأونلاين وتكون عبارة </h4>
                    </div>
                    <div class="col-xs-12">

                        <div class="col-lg-6 col-xs-6">

                            <div class="col-xs-3">
                                <label>  إدخال المدة  بالدقائق    </label>
                                <input class="form-control number" oninput="validity.valid||(value='');" type="number" style="margin: 15px auto" name="duration_book_online_first"
                                       value="{{ $setting->getBody('duration_book_online_first') }}" placeholder="بالدقائق"
                                       maxlength="500" readonly>
                            </div>

                            <div class="col-xs-3">
                                <label>  سعرها  </label>
                                <input class="form-control number" oninput="validity.valid||(value='');" type="number" style="margin: 15px auto" name="value_book_online_first"
                                       value="{{ $setting->getBody('value_book_online_first') }}" placeholder="سعرها"
                                       maxlength="500" >
                            </div>
                        </div>

                        <div class="col-lg-6 col-xs-6">

                            <div class="col-xs-3">
                                <label>  إدخال المدة  بالدقائق    </label>
                                <input class="form-control number" oninput="validity.valid||(value='');" type="number" style="margin: 15px auto" name="duration_book_online_second"
                                       value="{{ $setting->getBody('duration_book_online_second') }}" placeholder="بالدقائق"
                                       maxlength="500" readonly>
                            </div>

                            <div class="col-xs-3">
                                <label>  سعرها  </label>
                                <input class="form-control number" oninput="validity.valid||(value='');" type="number" style="margin: 15px auto" name="value_book_online_second"
                                       value="{{ $setting->getBody('value_book_online_second') }}" placeholder="سعرها"
                                       maxlength="500" >
                            </div>
                        </div>

                    </div>





                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>
@endsection


@section('scripts')
    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">

        $('form').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(this);
            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid()) {
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        //  $('#messageError').html(data.message);

                        var shortCutFunction = 'success';
                        var msg = ' تم تحديث الإعدادات بنجاح';
                        var title = 'نجاح';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                        {{--setTimeout(function () {--}}
                        {{--window.location.href = '{{ route('categories.index') }}';--}}
                        {{--}, 3000);--}}
                    },
                    error: function (data) {

                    }
                });
            }
        });

    </script>
@endsection




