@extends('admin.layouts.master')
@section('title', 'إدارة الإشتراكات ')
@section('content')

    <!-- Page-Title -->
    <div class="row zoomIn">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">


            </div>
            <h4 class="page-title"> قائمة     {{$pageName}} </h4>
        </div>
    </div>


    <div class="row zoomIn">

        <div class="col-sm-12">
            <div class="card-box rotateOutUpRight ">

                <table id="datatable-responsive" class="table  table-striped">
                    <thead>
                    <tr>
                        <th>إسم صاحب الطلب   </th>
                        <th>إسم  الدكتور   </th>
                        <th>    نوع الطلب     </th>
                        <th>القسم     </th>
                        <th>سعر     </th>
                        <th>مدفوع     </th>
                        <th>تاريخ الطلب   </th>
                        <th>      حالة الطلب    </th>
                        <th>      سبب رفض الطلب    </th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($reports as $report)

                        <tr>

                            <td> {{ optional($report->user)->name}}  </td>
                            <td> {{ optional($report->doctor)->name}}  </td>
                            <td> {{ $report->defined_order == "draw" ? "تحليل بالرسومات" : "حجز موعد"}}  </td>
                            <td> {{ optional($report->category)->name_ar}}  </td>
                            <td> {{$report->price}}  </td>
                            <td> {{$report->is_pay == 1 ?"مدفوع" :"غير مدفوع"}}  </td>
                            <td> {{ \Carbon\Carbon::parse($report->date_time)->format("Y-m-d")}}  </td>
                            <td>
                                @switch( $report->status)
                                    @case('refuse_system')
                                        تم رفضه من الإدراة
                                    @break
                                    @case('pending')
                                        جاري
                                    @break
                                    @case('accepted')
                                        تم الموافقه عليه
                                    @break
                                    @case('finish')
                                         منتهي
                                    @break
                                    @case("refuse_doctor")
                                    تم رفضه من الدكتور
                                    @break
                                    تم رفضه من المستخدم
                                    @default
                                @endswitch
                            </td>
                            <td> {{$report->message ? : "لا يوجد سبب"}}  </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- End row -->
@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
            $('#datatable-responsive').DataTable( {
                "order": [[ 2, "desc" ]]
            } );

        });
    </script>


@endsection

