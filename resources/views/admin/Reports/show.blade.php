@extends('admin.layouts.master')
@section('title', 'إدارة التقارير ')
@section('content')

    <!-- Page-Title -->
    <div class="row zoomIn">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">


            </div>
            <h4 class="page-title">       {{$pageName}} </h4>
        </div>
    </div>


    <div class="row zoomIn">

        <div class="col-sm-12">
            <div class="card-box rotateOutUpRight ">


                <table id="datatable-fixed-header" class="table  table-striped">
                    <thead>
                    <tr>
                        <th>رقم الطلب</th>
                        <th>اسم المستخدم  </th>
                        <th>رقم المستخدم  </th>
                        <th>حالة الضيف    </th>
                        <th>رقم الطاولة  </th>
                        <th>  معلومات توضحية  </th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($reports as $report)

                        <tr>
                            <td> {{$report->id}}  </td>
                            <td> {{$report->name}}  </td>
                            <td> {{$report->phone}}  </td>
                            <td> {{$report->guest_status == 'normal' ? 'عادي' : $report->guest_status}}  </td>
                            <td> {{$report->table_number}}  </td>
                            <td> {{$report->additional_information}}  </td>

                        </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- End row -->
@endsection

@section('scripts')

    <script>


    </script>


@endsection

