<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {

    $degree = ['الزمالة' ,'البكالوريوس' , 'الماجيستير' ,'الدكتوراه'];

    $random_degree = array_rand($degree,3);

    $images = [
        'https://dzf8vqv24eqhg.cloudfront.net/userfiles/2086/3660/ckfinder/images/7(4).jpg',
        'https://binaries.templates.cdn.office.net/support/templates/en-us/lt04027254_quantized.png'
    ];
    return [
        'degree' => $degree[$random_degree[0]],
        'experience_year' => rand(10,25),
        'age' => rand(30,80),
        'certificates' => $images,
        'desc' => $faker->text(),
    ];
});
