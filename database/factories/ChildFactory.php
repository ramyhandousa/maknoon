<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Child::class, function (Faker $faker) {
    $degree = ['dad' ,'mother' , 'teacher' ,'other'];

    $random_degree = array_rand($degree,3);

    $names = ['محمد' ,'أحمد' , 'عمر' ,'سعد'];

    $random_names = array_rand($names,3);

    return [
        'name' =>  $names[$random_names[0]],
        'image' => 'https://img.freepik.com/free-photo/infant-child-with-toy-airplane-his-hands-sunglasses-tourist_90756-210.jpg?size=626&ext=jpg&ga=GA1.2.1562791348.1604620800',
        'relationship' => $degree[$random_degree[0]] ,
        'relationship_other' => $degree[$random_degree[0]] == 'other' ?  $faker->title : null ,
        'age' => rand(1,10),
        'number_family' => rand(1,7) ,
    ];
});
