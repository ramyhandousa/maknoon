<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {

    return [
        'defined_user'      => 'doctor',
        'name'              => $faker->name,
        'city_id'           => rand(3,8),
        'phone'             => '0522522'.substr(rand(), 0, 4),
        'email'             => $faker->unique()->safeEmail,
        'image'             => "doctor6.jpg",
        'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'is_active'         => 1,
        'is_accepted'       => $faker->boolean,
        'subscribe_draw'    => $faker->boolean,
        'api_token'         => Str::random(60),
        'remember_token'    => Str::random(10),
    ];
//    return [
//        'defined_user'      => 'user',
//        'name'              => $faker->name,
//        'city_id'           => rand(3,8),
//        'phone'             => '052252'.substr(rand(), 0, 5),
//        'email'             => $faker->unique()->safeEmail,
//        'image'             => "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSCG7_AGYcFT_AXYmJGTNUbVDEG_ucXnoK1vQ&usqp=CAU",
//        'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
//        'is_active'         => 1,
//        'is_accepted'       => $faker->boolean,
//        'subscribe_draw'    => $faker->boolean,
//        'api_token'         => Str::random(60),
//        'remember_token'    => Str::random(10),
//    ];
});
