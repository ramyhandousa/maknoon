<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\User::class, 2)->create()->each(function ($user) {

            $user->profile()->save(factory(\App\Models\Profile::class)->make());

            $user->categories()->save(\App\Models\Category::whereId(rand(1,11))->first());

           $this->createDays($user);

        });

//        factory(App\User::class, 1)->create()->each(function ($user) {
//
//            $user->children()->save(factory(\App\Models\Child::class)->make())->each(function ($child){
//                $this->createQuestion($child);
//            });
//
//        });
    }

    public function createDays( $user){
        $days = \App\Models\Day::all();
        foreach ($days as $day){
            $data = [
                'user_id'   => $user->id,
                'day_id'    => $day->id,
                'start'     => date("H:i:s"),
                'end'       => date("H:i:s"),
                'working'   => rand(0,1),
            ];
            \App\Models\DoctorDay::create($data);
        }
    }

    public function createQuestion($child){
        $questions = \App\Models\Question::all();

        $data = [];
        foreach ($questions as $question){

            $data[] = [
                'child_id'      => $child->id,
                'question_id'   => $question->id,
                'checked'       => rand(0,1),
            ];
        }
        \Illuminate\Support\Facades\DB::table('question_user')->insert($data);
    }
}
