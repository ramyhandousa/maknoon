<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->enum('defined_user',['admin','user','doctor'])->default('user');
            $table->string('name');
            $table->string('code_phone');
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->integer('city_id')->nullable();
            $table->string('image')->nullable();
            $table->boolean('subscribe_draw')->default(0);
            $table->boolean('is_active')->default(0);
            $table->boolean('is_accepted')->default(0);
            $table->boolean('is_payed')->default(0);
            $table->boolean('is_suspend')->default(0);
            $table->string('message')->nullable();
            $table->string('lang',3)->default('ar');
            $table->string('api_token',60);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
