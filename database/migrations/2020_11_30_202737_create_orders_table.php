<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->enum('defined_order',['appointment','draw'])->default('appointment');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('doctor_id');
            $table->unsignedBigInteger('child_id')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->timestamp('date_time')->nullable();
            $table->unsignedFloat('price');
            $table->string('period')->nullable();
            $table->text('images')->nullable();
            $table->string('app_percentage')->nullable();
            $table->boolean('is_pay')->default('0');
            $table->boolean('time_out')->default('0');
            $table->string('paymentId')->nullable();
            $table->enum('status', ['pending', 'accepted','finish','refuse_system','refuse_doctor','refuse_user'])->default('pending');
            $table->text('message')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('doctor_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('child_id')->references('id')->on('children')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
