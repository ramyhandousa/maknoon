<?php

namespace App;

use App\Models\Category;
use App\Models\Child;
use App\Models\City;
use App\Models\Conversation;
use App\Models\Day;
use App\Models\Device;
use App\Models\DoctorDay;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Profile;
use App\Models\Question;
use App\Models\Rate;
use App\Traits\CanBeScoped;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Silber\Bouncer\Database\HasRolesAndAbilities;

use App\Notifications\MyAdminResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable  , HasRolesAndAbilities , CanBeScoped;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));

    }

    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function hasAnyRoles()
    {
        if (auth()->check()) {

            if (auth()->user()->roles->count()) {
                return true;
            }

        } else {
            redirect(route('admin.login'));
        }
    }



    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }


    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function days(){
        return $this->belongsToMany(Day::class,'doctor_days','user_id','day_id')->withPivot(['start','end','working']);
    }

    public function days_working(){
        return $this->hasMany(DoctorDay::class);
    }

    public function children(){
        return $this->hasMany(Child::class);
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'category_doctor','user_id','category_id');
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }


    public function doctor_orders(){
        return $this->hasMany(Order::class,'doctor_id');
    }

    public function my_rate(){
        return $this->hasMany(Rate::class,'doctor_id')->average('value');
    }

    public function ratings(){
        return $this->hasMany(Rate::class,'doctor_id');
    }

    public function conversations()
    {
        return $this->belongsToMany(Conversation::class) ;
    }
}
