<?php

namespace App\Listeners;

use App\Events\ChattingUserNotify;
use App\Libraries\PushNotification;

class ChattingUserNotifyListener
{
    public $push;

    public function __construct( PushNotification $push)
    {
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  ChattingUserNotify  $event
     * @return void
     */
    public function handle(ChattingUserNotify $event)
    {
        $conversation = $event->conversation;

        $conversation->load('users');
        $sender = $conversation->users->where('id','!=',auth()->id())->first();

        $devices = $sender->devices  ? $sender->devices->pluck('device') : null;

        if(count($devices ) > 0  ) {

            $this->push->sendPushNotification($devices, null, $event->user->name,$event->request->message ,
                [
                    'id'                =>  $event->message->id,
                    'user_id'           =>  $event->user->id ,
                    'user_image'        =>  $event->user->image != null ? \URL::to('/') .   $event->user->image : null,
                    'conversation_id'   =>  $conversation->id ,
                    'orderId'           =>  $conversation->order_id,
                    'message'           =>  $event->request->message ,
                    'type'              => 12
                ]
            );
        }
    }
}
