<?php

namespace App\Listeners;

use App\Events\AcceptAdminPaymentNotify;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Device;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AcceptAdminPaymentNotifyListener
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  AcceptAdminPaymentNotify  $event
     * @return void
     */
    public function handle(AcceptAdminPaymentNotify $event)
    {

        $devices = Device::where('user_id', $event->user->id)->pluck('device');

        $notify =    $this->notify->NotificationDbType(23,$event->user,null,$event->request,$event->order);

        if(count($devices ) > 0  ) {

            $this->push->sendPushNotification($devices, null, $notify['title'], $notify['body'],
                [
                    'id'            => $notify['id'],
                    'orderId'       => $notify['order_id'],
//                    'order_type'       => $event->orderStatus,
                    'type'          => $notify['type'],
                    'is_read'       => $notify['is_read'],
                    'title'         => $notify['title'],
                    'body'          => $notify['body'],
                    'created_at'    => $notify['created_at'],
                ]
            );
        }
    }
}
