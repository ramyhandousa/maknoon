<?php

namespace App\Listeners;

use App\Events\PhoneOrEmailChange;
use App\Jobs\ProcessMailSend;
use App\Mail\codeActivation;
use App\Models\VerifyUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class PhoneOrEmailChangeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PhoneOrEmailChange  $event
     * @return void
     */
    public function handle(PhoneOrEmailChange $event)
    {
        // $event->change => what will Be Change Phone Or E-mail

        $code_country =   optional($event->user->code)->code_country;

        $data = [ $event->change => $event->request , 'action_code'  => $event->code ,'code_country' => request()->code_country ? : $code_country];

        $user =  VerifyUser::updateOrCreate(['user_id' => $event->user->id], $data);

        if ($event->change === 'email'){

           // Sending E-mail
              ProcessMailSend::dispatch($user ,$event->code , codeActivation::class);

//            Mail::to($event->request)->send(new codeActivation($event->user, $event->code));

        }else{

            // Sms
                //Sms::sendMessage('Activation code:' . $event->code, $event->request);
            return;
        }

    }
}
