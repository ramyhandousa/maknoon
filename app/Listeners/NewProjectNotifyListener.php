<?php

namespace App\Listeners;

use App\Events\NewProjectNotify;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Device;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class NewProjectNotifyListener
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  NewProjectNotify  $event
     * @return void
     */
    public function handle(NewProjectNotify $event)
    {

        $devices = Device::where('user_id', $event->request->doctor_id)->pluck('device');

        $type_id =  $event->request->defined_order === "appointment" ? 6 : 7 ;

        $notify =   $this->notify->NotificationDbType($type_id,$event->request->doctor_id,$event->user, $event->request,$event->order);

        $new_order_translation = trans('global.new_order');

        if(count($devices ) > 0  ) {

            $this->push->sendPushNotification($devices, null, $notify['title'], $new_order_translation . $notify['body'],
                [
                    'id'            => $notify['id'],
                    'orderId'       => $notify['order_id'],
                    'type'          => $notify['type'],
                    'is_read'       => $notify['is_read'],
                    'title'         => $notify['title'],
                    'body'          => $new_order_translation . $notify['body'],
                    'created_at'    => $notify['created_at'],
                ]
            );
        }
    }
}
