<?php

namespace App\Listeners;

use App\Events\OrderStatus;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Device;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class OrderStatusListener
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  OrderStatus  $event
     * @return void
     */
    public function handle(OrderStatus $event)
    {
//        $id = $event->status == 8   ? $event->sender->id : $event->user->id;
        $devices = Device::where('user_id', $event->user->id )->pluck('device');

        $notify =    $this->notify->NotificationDbType($event->status,$event->user,$event->sender,$event->request,$event->order);

        if(count($devices ) > 0  ) {

            $this->push->sendPushNotification($devices, null, $notify['title'], $notify['body'],
                [
                    'id'            => $notify['id'],
                    'type'          => $notify['type'],
                    'orderId'       => $notify['order_id'],
                    'is_read'       => $notify['is_read'],
                    'title'         => $notify['title'],
                    'body'          => $notify['body'],
                    'created_at'    => $notify['created_at'],
                ]
            );
        }
    }
}
