<?php

namespace App\Listeners;

use App\Events\NewOrderOffer;
use App\Events\NewProjectNotify;
use App\Models\OrderOffer;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NewOrderOfferListener
{



    public function handle(NewOrderOffer $event)
    {
        $designers = User::whereDefinedUser('designer')->whereIsAccepted(1)->whereIsActive(1)
            ->whereIsSuspend(0)->whereHas('profile')->get(['id']);

        foreach ($designers as $designer){

            $order_offer = new OrderOffer();
            $order_offer->order_id = $event->order->id;
            $order_offer->provider_id = $designer->id;
            $order_offer->save();

            event(new NewProjectNotify($event->user,$designer,$event->order,$event->request));
        }
    }
}
