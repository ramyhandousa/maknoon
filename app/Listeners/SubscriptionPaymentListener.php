<?php

namespace App\Listeners;

use App\Events\SubscriptionPayment;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Device;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class SubscriptionPaymentListener
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }


    public function handle(SubscriptionPayment $event)
    {

        $devices = Device::where('user_id', $event->user->id )->pluck('device');

        $notify =    $this->notify->NotificationDbType($event->status,$event->user,null,$event->request->refuse_reason);

        if(count($devices ) > 0  ) {

            $this->push->sendPushNotification($devices, null, $notify['title'], $notify['body'],
                [
                    'id'            => $notify['id'],
                    'type'          => $notify['type'],
                    'title'         => $notify['title'],
                    'body'          => $notify['body'],
                    'created_at'    => $notify['created_at'],
                ]
            );
        }
    }
}
