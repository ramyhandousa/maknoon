<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Lists\CategoryResource;
use App\Http\Resources\Lists\CityResource;
use App\Http\Resources\Lists\QuestionsResources;
use App\Models\Category;
use App\Models\City;
use App\Models\Question;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class ListController extends Controller
{
    use  RespondsWithHttpStatus, paginationTrait;
    public  $lang;
    public function __construct( )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);

    }


    public function categories(){

        $categories = Category::select('id','name_'.$this->lang)->whereIsSuspend(0)->get();

        $data = CategoryResource::collection($categories);

        return $this->success('الأقسام',$data);
    }

    public function cities(Request  $request){

        $cities = City::select('id','name_'.$this->lang)->whereIsSuspend(0)->get();

        $data = CityResource::collection($cities);

        return $this->success('  المدن',$data);
    }

    public function questions(Request  $request){

        $cities = Question::select('id','name_'.$this->lang)->whereIsDeleted(0)->get();

        $data = QuestionsResources::collection($cities);

        return $this->success('  الأسئلة المطروحة من الأدمن',$data);
    }

}
