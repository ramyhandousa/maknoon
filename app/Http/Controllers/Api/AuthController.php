<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\Auth\changePassRequest;
use App\Http\Requests\api\Auth\checkPhoneOrEmailExist;
use App\Http\Requests\api\Auth\editDoctor;
use App\Http\Requests\api\Auth\editUser;
use App\Http\Requests\api\Auth\login;
use App\Http\Requests\api\Auth\register_doctor;
use App\Http\Requests\api\Auth\remove_user_image;
use App\Http\Requests\api\Auth\resgister;
use App\Http\Requests\api\removeImage;
use App\Http\Requests\api\validImage;
use App\Http\Resources\Doctor\DoctorResource;
use App\Http\Resources\User\UserResource;
use App\Models\Day;
use App\Models\Device;
use App\Models\DoctorDay;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class AuthController extends Controller
{

    use  RespondsWithHttpStatus;
    public  $lang;
    public  $path;
    public function __construct( )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);

        $this->path = 'files/';

        $this->middleware('auth:api')
            ->only([ 'changPassword' ,'active_user' ,'accepted_user' ,'edit_user_profile',
                    'edit_doctor_profile','removeUserImage']);
    }

    public function register(resgister $request)
    {
        $user = User::create($request->validated());

        $this->manageDevices($request, $user);

        $data=  ['api_token' => $user->api_token];

        return $this->success(trans('global.register_in_successfully') , $data);
    }

    public function register_doctor(register_doctor $request){

        $user =  User::create($request->validated()['user']);

        $user->profile()->create($request->validated()['profile']);

        $user->categories()->attach($request->categories);

        $this->insert_days($user);

        $this->manageDevices($request, $user);

        return $this->success(trans('global.register_in_successfully') , ['api_token' => $user->api_token]);
    }

    public function login(login  $request){

        $user = User::whereApiToken($request->api_token)->first();

        if ($user->defined_user == 'doctor'){

            $data = new DoctorResource($user);
        }else{

            $data = new UserResource($user);
        }

        $this->manageDevices($request, $user);

        return $this->success(trans('global.logged_in_successfully'), $data);
    }

    public function active_user(){

        $user = Auth::user();

        $user->update(['is_active' => 1]);

        return $this->success('تم التفعيل بنجاح');
    }

    public function accepted_user(){

        $user = Auth::user();

        $user->update(['is_accepted' => 1]);

        return $this->success('تم الموافقة بنجاح');
    }

    public function edit_user_profile (editUser $request )
    {
        $user =  $request->user();
        $user->fill($request->validated());
        $user->save();

        return $this->success(trans('global.profile_edit_success') ,  new UserResource($user));
    }

    public function edit_doctor_profile (editDoctor $request )
    {

        $user  =  $request->user();
        $user->fill($request->validated()['user']);
        $user->save();

        $user->profile()->update($request->validated()['profile']);

        if ($request->categories){
            $user->categories()->sync($request->categories);
        }
        if ($request->days){
            $user->days()->syncWithoutDetaching($this->edit_days($request));
        }

        return $this->success(trans('global.profile_edit_success') , new DoctorResource($user));
    }


    public function changPassword(changePassRequest  $request){
        $user = Auth::user();

        if ($request->newPassword){

            $user->update( [ 'password' => $request->newPassword] );

            $message =  trans('global.password_was_edited_successfully') ;

        }else{

            $message = trans('global.password_not_edited');
        }
        return $this->success($message);
    }

    public function check_data(checkPhoneOrEmailExist  $request){

        return $this->success('تستطيع التسجيل بتلك البيانات');
    }

    public function upload(validImage $request){

        if ($request->file('image') ||$request->file('audio')  ){
            $file = $request->image ? : $request->audio;

            $imageName  =  Str::random(20) .$file->getClientOriginalName();
            $file->move(public_path('files'), $imageName);

            $filename = $this->path.$imageName;

            return $this->success(' الملف', ['file' => $filename]);
        }
    }

    public function removeImage(removeImage  $request){

//        Log::info("file Remove    " . $request->image);

        if ( File::delete(public_path().'/'.$request->image)){
            return $this->success('تم مسح الملف بنجاح');
        }

        return $this->failure('غالبا مسار الملف غير صحيح');
    }

    public function removeUserImage(remove_user_image $request){

        $user = Auth::user();

        $user->update(['image' => null]);

        return $this->success( ' بنجاح  '. $user->name .' تم مسح صورة ');
    }


    public function removeUser(Request  $request){

        $user = User::wherePhone($request->phone)->first();

        $user->delete();

        return $this->success('طيرت امه ولا يمهك ياعم عمر ');
    }


    private function insert_days($user){

        $days = Day::all();
        $data = [];
        foreach ($days as $day){
            $data[] = [
                'user_id'   => $user->id,
                'day_id'    => $day->id,
                'start'     => "10:00:00",
                'end'       => "22:00:00",
                'working'   => 1,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ];
        }
        DoctorDay::insert($data);

    }



    public function edit_days($request){
        return collect($request->days)->keyBy('id')->map(function ($day)use ($request){
           return [
               'user_id'    => $request->user()->id,
               'day_id'     => $day['id'],
               'start'      => $day['start'],
               'end'        => $day['end'],
               'working'    => $day['working'],
           ] ;
        });
    }


    public function manageDevices($request, $user)
    {
        if ($request->deviceToken) {

            Device::updateOrCreate(['device' => $request->deviceToken], ['user_id' => $user->id]);
        }
    }

}
