<?php

namespace App\Http\Controllers\Api;

use App\Events\ChattingUserNotify;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\message\storeVaild;
use App\Http\Resources\Messages\showConversationResource;
use App\Http\Resources\Messages\showMessagesResource;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\Order;
use App\Traits\RespondsWithHttpStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    use RespondsWithHttpStatus;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $user = Auth::user()->load(['conversations.users','conversations.last_messages']);

        $conversations = $user->conversations->indexData();

//        $pageSize = $request->pageSize;
//
//        $currentPage = $request->get('page', 1);
//
//        $data = $conversations->slice($request->skipCount + (($currentPage - 1) * $pageSize))->take($pageSize)->values();
//

        $data = $conversations;

        return $this->success('المحادثات', $data);
    }

    public function store(storeVaild $request)
    {
        $order = Order::findOrFail($request->order_id);

         $conversation = Conversation::firstOrCreate(['order_id' => $request->order_id],['order_id' => $request->order_id]);

         if ($conversation->users->count() ==  0){
             $conversation->users()->attach([$order->user_id , $order->doctor_id]);
         }

        $message = Message::create(['user_id' => Auth::id(), 'conversation_id' => $conversation->id,
            'message' => $request->message,'url' => $request->url]);

        $user = Auth::user()->load('conversations.users');

        event(New ChattingUserNotify($conversation,$user,$message,$request));

        return $this->success('تم الإرسال بنجاح', $message);
    }


    public function show(Request $request,  $id)
    {
        $order = Order::findOrFail($id);

        $conversation = Conversation::where('order_id',$order->id)->with('users','last_messages')->first();
        if (!$conversation){
            return $this->success('الشات');
        }
        $users = $conversation->users;

        $user = $users->where('id',auth()->id())->first();


        $sender = $users->where('id','!=',auth()->id())->first();

        if ($user){ // make all conversation read
            $user->pivot->update(['read_at' => Carbon::now()]);
        }

        $last_messages = $conversation->last_messages->whereIn('user_id',$conversation->users()->pluck('id'));

        $pageSize = $request->pageSize;
        $skipCount = $request->skipCount;

        if ($pageSize || $request->skipCount){

            $currentPage = $request->get('page', 1);

            $data = $last_messages->slice($skipCount + (($currentPage - 1) * $pageSize))->take($pageSize)->values();

        }else{
//            $data = $last_messages->take(10);
            $data = $last_messages;
        }

        return $this->success('الشات', new showMessagesResource($data ,$sender ));
    }


    public function complete(Conversation $conversation)
    {
        $conversation->load('order');

        $order = $conversation['order'];

        $this->authorize('complete', $order);

        $conversation->update(['status' => 'close']);

        $order->update(['status' => 'finish']);

        return $this->success('تم إعتماد طلبك بنجاح');
    }
}
