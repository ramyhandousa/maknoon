<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Doctor\DoctorResource;
use App\Http\Resources\User\UserResource;
use App\Models\Notification;
use App\Models\Order;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    use  RespondsWithHttpStatus , paginationTrait;
    public  $lang;
    public  $path;
    public function __construct( )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);

        $this->path = 'files/';

        $this->middleware('auth:api')->only([ 'payment_doctor','test_payment' ]);
    }


    public function show($id){
        $user = User::where('defined_user','!=','admin')->findOrFail($id);

        if ($user->defined_user == 'doctor'){

            $data = new DoctorResource($user);
        }else{

            $data = new UserResource($user);
        }
        return $this->success('تفاصيل المستخدم', $data);
    }

    public function payment_doctor(){
        $user = Auth::user();


        if ($user === "user"){
            return $this->failure('لا يوجد حساب او رصيد لك');
        }

        $orders = Order::whereDoctorId($user->id)
            ->where('status','finish')
            ->select(DB::raw('sum(Case When app_percentage IS NULL Then price Else price - (price * app_percentage / 100 )  End ) as doctor,
                        sum(Case When app_percentage IS NULL Then "0" Else  price * app_percentage / 100   End ) as admin
                '))
            ->first();


        return  $this->success('العمولة', $orders);
    }

    function test_payment(){
        $user = Auth::user();


        $orders = Order::whereDoctorId($user->id)
            ->where('status','finish')
            ->select(DB::raw('sum(Case When app_percentage IS NULL Then price Else price - (price * app_percentage / 100 )  End ) as doctor,
                        sum(Case When app_percentage IS NULL Then "0" Else  price * app_percentage / 100   End ) as admin
                '))
            ->first();

        return $orders;
    }

}
