<?php

namespace App\Http\Controllers\Api;

use App\Events\ContactUs;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\message\storeVaild;
use App\Http\Requests\api\setting\bankTransferRequest;
use App\Http\Requests\api\setting\contactVaild;
use App\Http\Resources\Setting\BankResource;
use App\Http\Resources\Setting\typeSupportResource;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Bank;
use App\Models\BankTransfer;
use App\Models\Device;
use App\Models\Message;
use App\Models\Order;
use App\Models\Setting;
use App\Models\Support;
use App\Models\TypeSupport;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    public $push;
    public  $notify;
    public  $lang;
    use RespondsWithHttpStatus;
    public function __construct(InsertNotification $notification , PushNotification $push )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);
        $this->middleware('auth:api')->only(['contact_us','testingNotification']);
        $this->push = $push;
        $this->notify = $notification;
    }



    public function testingNotification(storeVaild $request){

        $user = Auth::user();
        $data =  $request->validated();

        $data['user_id']  = $user->id;

        $message = Message::create($data);

        $devices = Device::where('user_id', 45)->pluck('device');
       return $this->push->sendPushNotification($devices, null, $request->title? : 'المصمم الذكي',$request->message? : 'testing' ,
            [
                'id'                => $request->id,
                'user_id'           =>  $user->id ,
                'user_image'        =>  $request->user_image ,
                'conversation_id'   =>  $request->conversation_id ,
                'message'           =>  $request->message ,
                'orderId'           =>  $request->orderId ,
            ]
        );
    }


    public function reservation_setting(){
        $data_setting = ['value_book_draw','duration_book_online_first','value_book_online_first',
            'duration_book_online_second','value_book_online_second'];
        $setting = Setting::whereIn('key',$data_setting)->get();

        $draw = $setting->where('key','value_book_draw')->first()->body;

        $duration_online_first = $setting->where('key','duration_book_online_first')->first()->body;
        $value_online_first = $setting->where('key','value_book_online_first')->first()->body;
        $duration_online_second = $setting->where('key','duration_book_online_second')->first()->body;
        $value_online_second = $setting->where('key','value_book_online_second')->first()->body;

        $online = ['first' => ['duration'  => $duration_online_first, 'price' =>$value_online_first ] ,
                 'secound' =>  ['duration'  => $duration_online_second, 'price' => $value_online_second]];

        $data = ['draw' => $draw , 'reservation' => $online];

        return $this->success('الإعدادت العامة ' , $data);
    }

    public function aboutUs(){
        $about_us =  Setting::where('key','about_us_' .$this->lang)->first();

        $data = [
            "about_us"      => $about_us ? $about_us->body : '',
            "instagram"      => Setting::getBody('instagram'),
            "twitter"        => Setting::getBody('twitter'),
        ];

        return $this->success('عن التطبيق ', $data);
    }

    public function terms_user(){
        $setting = Setting::where('key','terms_user_'.$this->lang)->first();

        $data = [
            "about_us"      => $setting ? $setting->body : '',
            "instagram"      => Setting::getBody('instagram'),
            "twitter"        => Setting::getBody('twitter'),
        ];

        return $this->success( 'الشروط والأحكام', $data);
    }


    public function terms_doctor(){
        $setting = Setting::where('key','terms_doctor_'.$this->lang)->first();


        $data = [
            "about_us"      => $setting ? $setting->body : '',
            "instagram"      => Setting::getBody('instagram'),
            "twitter"        => Setting::getBody('twitter'),
        ];

        return $this->success( 'الشروط والأحكام للأخصائين', $data);
    }


    public function getTypesSupport(){

        $types = TypeSupport::all();

        $data= typeSupportResource::collection($types);

        return $this->success( 'أنواع التواصل', $data);
    }

    public function contact_us(contactVaild $request ){
        $user = Auth::user();

        $support = new Support();
        $support->user_id = 1;
        $support->sender_id = $user->id;
        $support->type_id = (int) $request->type;
        $support->message = $request->message;
        $support->save();

        event(new ContactUs(1,$user->id,$request));

        return $this->success( trans('global.message_was_sent_successfully'));
    }

}
