<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;
use App\Models\Notification;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    use  RespondsWithHttpStatus , paginationTrait;
    public  $lang;

    public function __construct( )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);

        $this->middleware('auth:api');
    }

    public function index(Request  $request)
    {
        $notifications = Notification::whereUserId(Auth::id());

        $total_count = $notifications->count();

        $this->pagination_query($request, $notifications);

        $data = NotificationResource::collection($notifications->get());

        $reading = $notifications->get()->each->update(['is_read' => 1]);

        return  $this->successWithPagination('كل الإشعارات' , $total_count, $data) ;
    }


    public function update(Request $request, $id)
    {
        $notification = Notification::findOrFail($id);

        $notification->update(['is_read' => 1]);

        return $this->success('تم القراءة بنجاح' );
    }


    public function destroy($id)
    {
        $notification = Notification::findOrFail($id);

        $notification->delete();

        return $this->success('تم مسح الإشعار بنجاح' );
    }


}
