<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Doctor\DoctorResource;
use App\Http\Resources\HomeIndex;
use App\Scoping\Scopes\BrandScope;
use App\Scoping\Scopes\CategoryScope;
use App\Scoping\Scopes\CityScope;
use App\Scoping\Scopes\ColourScope;
use App\Scoping\Scopes\NameScope;
use App\Scoping\Scopes\OfferScope;
use App\Scoping\Scopes\OrderByScope;
use App\Scoping\Scopes\PriceFromScope;
use App\Scoping\Scopes\PriceToScope;
use App\Scoping\Scopes\ProductPurchaseScope;
use App\Scoping\Scopes\SizeScope;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    use  RespondsWithHttpStatus ,paginationTrait;
    public  $lang;
    public  $path;
    public function __construct( )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);

        $this->path = 'files/';

        $this->middleware('auth:api')
            ->only([ 'changPassword' ,'editProfile','edit_doctor_profile','removeUserImage','removeUser']);
    }


    public function list_doctors(Request  $request){

        $query = User::whereDefinedUserAndIsAcceptedAndIsSuspend('doctor',1,0)
                    ->select('id','name','image')
                    ->withScopes($this->filterQuery())
                    ->withCount(['ratings as average_rating' => function($query) {
                        $query->select(DB::raw('coalesce(avg(value),0)'));
                    }])->orderByDesc('average_rating');

         $total_count = $query->count();

        $this->pagination_query($request,$query);

        $doctors = HomeIndex::collection($query->get());

        return $this->successWithPagination('الأخصائين', $total_count,$doctors);
    }

    public function show_doctor($id){

        $doctor = User::whereDefinedUser('doctor')->findOrFail($id);

        return $this->success('تفاصيل الأخصائي', new DoctorResource($doctor));
    }

    public function list_doctors_drawing(Request  $request){

        $doctors = User::whereDefinedUserAndIsAcceptedAndIsSuspend('doctor',1,0)
                    ->whereHas('categories',function ($category) use ($request){
                        $category->where('category_id',$request->category_id);
                    })
                    ->where('subscribe_draw',1)
                    ->select('id','name')->get();

        return $this->success('الأخصائين المشتركين في الرسومات' , $doctors);
    }

    protected function filterQuery(){
        return [
            'name'         => new NameScope(),
            'city'         => new CityScope(),
            'category'     => new CategoryScope(),
        ];
    }
}
