<?php

namespace App\Http\Controllers\Api;

use App\Events\NewProjectNotify;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\Order\AcceptedOrderVaild;
use App\Http\Requests\api\Order\FinishOrderVaild;
use App\Http\Requests\api\Order\OrderRateRequest;
use App\Http\Requests\api\Order\RefuseOrderVaild;
use App\Http\Requests\api\Order\storeRequest;
use App\Http\Requests\api\Order\updateOrderVaild;
use App\Http\Resources\Orders\OrderDetails;
use App\Http\Resources\Orders\OrderIndex;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Conversation;
use App\Models\Device;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Rate;
use App\Scoping\Scopes\OrderByScope;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    use  RespondsWithHttpStatus , paginationTrait;
    public  $lang;
    public  $path;
    public $notify;
    public $push;
    public function __construct(InsertNotification $notification,PushNotification $push )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);
        $this->notify = $notification;

        $this->path = 'files/';
        $this->push = $push;

        $this->middleware('auth:api');
    }

    public function index(Request  $request)
    {
        $user =  $request->user();

        if ($user->defined_user == 'user'){
            $order = Order::whereUserId($user->id);
        }else{
            $order = Order::whereDoctorId($user->id);
        }

        $query = $order->withScopes($this->filterQuery());

        $total_count = $query->count();

        $this->pagination_query_update_at($request, $query);

        return $this->successWithPagination(trans('global.orders'),$total_count , OrderIndex::collection($query->get()));
    }

    protected function filterQuery(){
        return [
            'filter' => new OrderByScope(),
        ];
    }


    public function store(storeRequest $request)
    {
        $order = $request->user()->orders()->create($request->validated());

        event(new NewProjectNotify($request->user(),$order,$request));

        return $this->success(trans('global.order_was_added_successfully') ,  new OrderIndex($order));
    }


    public function show($id)
    {
       $order = Order::findOrFail($id);

       return $this->success('تفاصيل الطلب', new  OrderDetails($order));
    }


    public function update(updateOrderVaild $request, $id)
    {
        $order = Order::findOrFail($id);

        if ($order->defined_order == "draw"){

            if ($order->doctor_id != $request->doctor_id){

                event(new NewProjectNotify($request->user(),$order,$request));
            }
            $order->update($request->validated());
        }else{

            $data = collect($request->validated())->only(['date_time',"price","period"])->toArray();

            $order->update($data);
        }

        return $this->success(trans('global.edit_order') ,  new OrderDetails($order));
    }

    public function destroy($id)
    {
        $order = Order::findOrFail($id);

        return $this->success( trans('global.delete_order'));
    }

    public function pay_order(Request $request,Order $order){
        if ($order->status !== 'accepted'){
            return  $this->failure('يجب انتظار موافقة الأخصائي  قبل الدفع');
        }
        if ($order->is_pay == 0 ){

            $order->update(['is_pay' => 1]);

            $devices = Device::whereUserId($order->doctor_id)->pluck('device');

            $this->push->sendPushNotification($devices,null,'  الطلبات',  ' تم دفع طلبك رقم ' . $order->id   ,[
                'orderId' => $order->id,
                'order_type' => $order->status
            ]);


            $this->notify->NotificationDbType(11,$order->doctor_id,$request->user(),$request,$order);

            return $this->success('تم دفع الطلب بنجاح');
        }
        return  $this->failure('تم دفع الطلب مسبقا');
    }


    public function refuse_order(RefuseOrderVaild $request , Order $order){

        $user = $request->user();

        $vaild = $order->doctor_id == $user->id;

        $data = [];
        if ($vaild){
            $data['message'] = $request->message;
            $data['status'] = "refuse_doctor";
            $devices = Device::whereUserId($order->user_id)->pluck('device');
            $user_id = $order->user_id;
        }else{
            $data['status'] =   "refuse_user";
            $devices = Device::whereUserId($order->doctor_id)->pluck('device');
            $user_id = $order->doctor_id;
        }
        $order->update($data) ;

        $this->push->sendPushNotification($devices,null,'  الطلبات',   ' تم رفض الطلب رقم ' . $order->id  ,[
            'orderId' => $order->id,
            'order_type' => $order->status
        ]);

        Notification::create(['user_id' => $user_id,'sender_id' =>  $user->id, 'title' => 'orders'  ,
            'translation'   => 'order_refuse_by',
            'body'          =>   $user->name
        ]);

        return $this->success(trans('global.refuse_order'),  new OrderIndex($order));
    }


    public function accepted_order(AcceptedOrderVaild $request , Order $order){

        $order->update(['status' => "accepted"]) ;

        $devices = Device::whereUserId($order->user_id)->pluck('device');

        $this->push->sendPushNotification($devices,null,'  الطلبات',   ' تم قبول طلبك رقم ' . $order->id  ,[
            'orderId' => $order->id,
            'order_type' => $order->status
        ]);

        $this->notify->NotificationDbType(9,$order->user_id,$request->user(),$request,$order);

        return $this->success(trans('global.accepted_order'),  new OrderIndex($order));
    }

    public function finish_order(FinishOrderVaild $request , Order $order){

        $vaild = $order->defined_order == "draw";

        $data = [];
        if ($vaild){
            $data['report_file'] = $request->report_file;
            $data['status'] = "finish";
        }else{
            $data['status'] =   "finish";
        }
        $order->update($data) ;

        $devices = Device::whereUserId($order->user_id)->pluck('device');

        $this->push->sendPushNotification($devices,null,'  الطلبات',   ' تم إنهاء طلبك رقم ' . $order->id  ,[
            'orderId' => $order->id,
            'order_type' => $order->status
        ]);

        $this->notify->NotificationDbType(10,$order->user_id,$request->user(),$request,$order);


        $conversation = Conversation::whereOrderId($order->id)->first();

        $conversation->update([ 'status' => 'close'  ]);

        return $this->success(trans('global.finish_order'),  new OrderIndex($order));
    }

    public function rate_order(OrderRateRequest $request , Order  $order){

        $user = $request->user();

        $rate = Rate::updateOrCreate(['user_id' =>  $user->id ,'order_id' => $order->id],[
            'doctor_id' => $order->doctor_id,
            'value'     => $request->value,
            'comment'   => $request->comment
        ]);

        $devices = Device::whereUserId($order->doctor_id)->pluck('device');

        $this->push->sendPushNotification($devices,null,' تقيم الطلبات',  ' تم تقييم طلبك من المستخدم ' . $user->name   ,[
            'orderId' => $order->id,
            'order_type' => $order->status
        ]);


        $this->notify->NotificationDbType(13,$order->doctor->id,$user,$request,$order);

        return $this->success('تم تقييم الطلب بنجاح', $rate);
    }
}
