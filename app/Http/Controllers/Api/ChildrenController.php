<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\Child\checkHaveChildrenVaild;
use App\Http\Requests\api\Child\editChildVaild;
use App\Http\Requests\api\Child\storeChildVaild;
use App\Http\Resources\User\ChildDetailResource;
use App\Http\Resources\User\ChildResource;
use App\Models\Child;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChildrenController extends Controller
{
    use  RespondsWithHttpStatus , paginationTrait;
    public  $lang;
    public  $path;
    public function __construct( )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);

        $this->path = 'files/';

        $this->middleware('auth:api');
    }

    public function index()
    {
        $user = Auth::user();

        $children = Child::whereUserId($user->id)->select('id','name','image','age')->get();

        return $this->success('الأطفال التابعين لديك',ChildResource::collection($children));
    }

    public function store(storeChildVaild $request)
    {
        $data =  collect($request->validated())->except('questions')->toArray();

        $child = $request->user()->children()->create($data);

        $child->questions()->sync($request->questions);

        return $this->success('تم إضافة الطفل بنجاح', new ChildResource($child));
    }


    public function show($id)
    {
        $child = Child::with('questions')->findOrFail($id);


        return $this->success('تفاصيل الطفل', new ChildDetailResource($child));
    }

    public function update(editChildVaild $request, $id)
    {
        $child = Child::findOrFail($id);

        $data =  collect($request->validated())->except('questions')->toArray();

        $child->update($data);

        $child->questions()->sync($this->syncQuestions($request));

        return $this->success('تم تعديل الطفل بنجاح', new ChildResource($child));
    }

    private function syncQuestions($request){
        return collect($request->questions)->keyBy('question_id')->map(function ($q) {
            return [
              'question_id'  => $q['question_id'],
              'checked'      => $q['checked'],
            ];
        });
    }

    public function destroy($id)
    {
        $child = Child::findOrFail($id);

        $child->delete();

        return $this->success('تم مسح الطفل');
    }


    public function check_have_children(checkHaveChildrenVaild $request){

        return $this->success("تستطيع إضافة طلبك الأن");
    }
}
