<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class QuestionController extends Controller
{
    public function index(Request $request)
    {
        $cities = Question::whereIsDeleted(0)->latest()->get();

        $pageName = 'الأسئلة المطروحة';

        return view('admin.Questions.index',compact('cities','pageName'));
    }

    public function create(Request $request)
    {
        $pageName = 'الأسئلة المطروحة';

        $cities = Question::latest()->get();

        return view('admin.Questions.create',compact('pageName','cities'));
    }

    public function store(Request $request)
    {

        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $city = new Question;
        $city->name_ar = $request->name_ar;
        $city->name_en = $request->name_en;
        $city->save();

        $name = 'الأسئلة المطروحة';
        $url =  route('questions.index');

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => $name]),
            "url" => $url,

        ]);
    }


    public function edit($id,Request $request)
    {
        $city = Question::findOrFail($id);

        $pageName = 'الأسئلة المطروحة';

        return view('admin.Questions.edit',compact('city','pageName'));
    }


    public function update(Request $request, $id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $city = Question::findOrFail($id);

        $city->name_ar = $request->name_ar;
        $city->name_en = $request->name_en;
        $city->save();

        $name = 'الأسئلة المطروحة';
        $url =  route('questions.index');

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => $name]),
            "url" => $url,

        ]);
    }

    public function delete(Request $request)
    {
        $model = Question::findOrFail($request->id);

        if ($model->update(['is_deleted' => 1])) {
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }

}
