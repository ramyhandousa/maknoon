<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\Countery;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class CitiesController extends Controller
{


    public function index(Request $request)
    {
        $cities = City::latest()->get();

        $pageName = 'المدن';

        return view('admin.cities.index',compact('cities','pageName'));
    }

    public function create(Request $request)
    {
        $pageName = 'المدن';

        $cities = City::latest()->get();

        return view('admin.cities.create',compact('pageName','cities'));
    }

    public function store(Request $request)
    {

        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $city = new City;
        $city->name_ar = $request->name_ar;
        $city->name_en = $request->name_en;
        $city->save();

        $name = 'المدينة';
        $url =  route('cities.index');

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => $name]),
            "url" => $url,

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id,Request $request)
    {
        $city = City::findOrFail($id);

        $pageName = 'المدينة';

        return view('admin.cities.edit',compact('city','pageName'));
    }


    public function update(Request $request, $id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $city = City::findOrFail($id);

        $city->name_ar = $request->name_ar;
        $city->name_en = $request->name_en;
        $city->save();

        $name = 'المدينة';
        $url =  route('cities.index');

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => $name]),
            "url" => $url,

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function groupDelete(Request $request)
    {

        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $ids = $request->ids;

        $arrsCannotDelete = [];
        foreach ($ids as $id) {
            $model = City::findOrFail($id);

            if ($model->companies->count() > 0) {
                $arrsCannotDelete[] = $model->name;
            } else {
                $model->delete();
            }
        }

        return response()->json([
            'status' => true,
            'data' => [
                'id' => $request->id
            ],
            'message' => $arrsCannotDelete
        ]);
    }



    public function delete(Request $request)
    {
        $model = City::findOrFail($request->id);

        if ($model->users->count() > 0) {
            return response()->json([
                'status' => false,
                'message' => "عفواً, لا يمكنك حذف المدينة لوجود مستخدمين بها"
            ]);
        }

        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }


    public function suspend(Request $request)
    {
        $model = City::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم الحظر على مستوي النظام بنجاح";

        } else {
            $message = "لقد تم فك الحظر  بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }


}
