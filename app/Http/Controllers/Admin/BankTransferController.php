<?php

namespace App\Http\Controllers\Admin;

use App\Events\AcceptAdminPaymentNotify;
use App\Events\RefuseAdminPaymentNotify;
use App\Http\Controllers\Controller;
use App\Jobs\SendSmsJob;
use App\Jobs\SendSmsQueue;
use App\Jobs\Testing;
use App\Models\BankTransfer;
use App\Models\ContactInvitation;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BankTransferController extends Controller
{
   public function index(){
       $banks = BankTransfer::latest()->with('invitation')->get();


       $pageName = 'التحويلات البنكية' ;

       return view('admin.bankTransfer.index',compact('banks','pageName'));
   }

    public function accepted(Request $request){

        $bank = BankTransfer::with('user','invitation')->whereId($request->id)->first();

        $order = $bank['invitation'];

        if ($order ){

            $order->update(['is_payed' => 1 ,'payment_money' => 0 ]);

            if ( ( $order->number_guests +  $bank->addition_number) > $order->number_guests ){ // GUESSES Want To Increase Number
                $order->update(['number_guests' => $order->number_guests +  $bank->addition_number ]);
            }

            $contact_invitation = ContactInvitation::select('id','phone','is_payed')->where('invitation_id',$order->id)->whereIsPayed(0)->get();

            $allPhones = $contact_invitation->pluck('phone');

            SendSmsJob::dispatch($order,$allPhones,'store');

            $contact_invitation->each->update(['is_payed' => 1]);
        }

        $bank->update(['is_accepted'=> 1]);

        Payment::where('bank_transfer_id' , $request->id)->where('is_review', 0)->update(['is_review' => 1]);

        event(new AcceptAdminPaymentNotify($bank['user'],$order,$request));
        return response()->json([
            'status'=>true,
            'title'=>"نجاح",
            'message'=>"تم قبول الطلب بنجاح",
        ]);
    }


    public function refuse(Request $request){

        $bank = BankTransfer::with('user','invitation')->whereId($request->id)->first();

        event(new RefuseAdminPaymentNotify($bank['user'],$bank['invitation'],$request));

        $bank->update(['is_accepted' =>  -1 ,'message' => $request->refuse_reason]);

        Payment::where('bank_transfer_id' , $request->id)->where('is_review', 0)->delete();


        session()->flash('success','تم رفض الطلب بنجاح');
        return redirect()->back();
    }

}
