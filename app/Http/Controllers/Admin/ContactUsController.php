<?php

namespace App\Http\Controllers\Admin;

use App\Events\ContactUs;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Support;
use App\Models\TypeSupport;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ContactUsController extends Controller
{
    public $push;

    public $notify;

    public function __construct(PushNotification $push,InsertNotification $notification)
    {

        $this->push = $push;
        $this->notify = $notification;

    }

    public function index(Request $request)
    {
        $query = new Support;

        $id = Auth::id();
        if ($request->type == 'sender'){

            $my_message =   $query->whereSenderId($id)->latest()->get();

        }elseif ($request->type == 'deleted'){

            $my_message =    $query->where('is_deleted',1 )->latest()->get();
        }else{

            $my_message =    $query->where('user_id',$id )->where('is_deleted',0 )->latest()->get();
        }


        $types  = TypeSupport::all();
        $dealers  = User::whereNotIn('id',[1, $id])->get(['id','name']);

        $messageNotReadCount = Support::whereUserId($id)->whereIsRead(0)->count();

        return view('admin.inbox.index', compact('types' ,'my_message','messageNotReadCount','dealers'));
    }



    public function updateIsRead(Request $request){
        $support = Support::find($request->id);
        $support->update([
            'is_read' => 1
        ]);
    }


    public function updateIsDeleted(Request $request){

        $support = Support::whereIn('id', $request->ids)->update([
            'is_deleted' => 1,
            'is_read' => 1
        ]);
        return response()->json([
            'status' => true,
            'message' => '  تم نقلها إالي صندوق المحذوفات ',
            'data' => $support

        ], 200);
    }


    public function removeAllMessages(Request $request){

        $support = Support::whereIn('id', $request->ids)->get();
        $support->each->delete();
        return response()->json([
            'status' => true,
            'message' => ' تم مسح الرسائل نهائيا',
            'data' => $support

        ], 200);
    }



    public function store(Request $request)
    {
       $support = new Support();
       $support->user_id = $request->userId;
       $support->sender_id = Auth::id();
       $support->type_id = $request->typeId ? : 1;
       $support->parent_id = $request->parentId ? : 0;
       $support->message = $request->message;
       $support->save();

       event(new ContactUs($request->userId,Auth::id(),$request));

        return response()->json([
            'status' => true,
            'message' => 'تم إراسل رسالتك بنجاح ',
            'data' => $support

        ], 200);
    }

}
