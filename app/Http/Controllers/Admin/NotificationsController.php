<?php

namespace App\Http\Controllers\Admin;


use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications;
use App\User;
use App\Models\Device;
use App\Libraries\Main;
use App\Libraries\PushNotification;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class NotificationsController extends Controller
{


    public $push;
    public $main;

    public function __construct(PushNotification $push, Main $main)
    {

        $this->push = $push;
        $this->main = $main;

        Carbon::setLocale(config('app.locale'));

    }

    public function index()
    {
         $notifications = auth()->user()->notifications()->orderBy('created_at', 'desc')->get();

        return view('admin.notifications.index')->with(compact('notifications'));
    }

    public function publicNotifications()
    {

        $notifications = Notification::whereType(15)->with('user')->get();

        return view('admin.notifications.list')->with(compact('notifications'));

    }


    public function createPublicNotifications()
    {
        return view('admin.notifications.public');
    }


    public function sendPublicNotifications(Request $request)
    {
//        Notification::create([ 'topic' => 'all','title'=>  $request->title,'body'=> $request->message ,'type'=> 15]);

        $data = [
            'title'=>  $request->title,
            'body'=> $request->message
        ];

        $this->push->sendPushNotification(null, null, $request->title,$request->message, $data,'topic' );
        session()->flash('success', __('trans.successSendPublicNotifications'));

       return redirect()->back();
    }


    public function getNotifications()
    {
        $user = auth()->user();
        $notifications = $user->notifications;
        return view('admin.notifications.index')->with(compact('notifications'));

    }


    public function delete(Request $request)
    {

        $model = Notification::whereId($request->id)->first();


        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => $request->id
            ]);
        }

    }


}
