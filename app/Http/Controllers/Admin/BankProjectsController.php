<?php

namespace App\Http\Controllers\Admin;

use App\Events\AcceptAdminPaymentNotify;
use App\Events\RefuseAdminPaymentNotify;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\BankTransfer;
use App\Models\Conversation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankProjectsController extends Controller
{
    public $notify;
    public $push;

    public function __construct( InsertNotification $notification,PushNotification $push)
    {

        $this->notify = $notification;
        $this->push = $push;
    }

    public function index()
    {
        $banks = BankTransfer::where('order_type','project')->where('is_accepted',0)->with('orderProject')->latest()->get();

        $pageName = 'إدارة طلبات المشاريع';

        return view('admin.bankTransfer.projects.index',compact('banks' ,'pageName'));
    }

    public function create()
    {
        $banks = BankTransfer::where('order_type','project')->where('is_accepted','!=',0)->with('orderProject')->latest()->get();

        $pageName = 'أرشيف طلبات المشاريع';

        return view('admin.bankTransfer.projects.show',compact('banks' ,'pageName'));
    }

    public function accepted(Request $request){

        $bank = BankTransfer::with('user','orderProject')->whereId($request->id)->first();


        $bank->update(['is_accepted'=> 1]);
        $bank->orderProject->update(['is_review'=> 1 ,'is_pay' => 1]);

        //$this->notify->NotificationDbType(23,$bank['user'],null,$request,$bank['orderProject']);
        event(new AcceptAdminPaymentNotify($bank['user'],$bank['orderProject'],$request));

        $conversation = new Conversation();
        $conversation->project_order_id = $bank['orderProject']->id;
        $conversation->save();
        $conversation->users()->attach([$bank['user']->id, $bank['orderProject']->provider_id]);

        return response()->json([
            'status'=>true,
            'title'=>"نجاح",
            'message'=>"تم قبول الطلب بنجاح",
        ]);
    }

    public function refuse(Request $request){

        $bank = BankTransfer::with('user','orderProject')->whereId($request->id)->first();

        $bank->update(['is_accepted' =>  -1 ,'message' => $request->refuse_reason]);
        $bank->orderProject->update(['status'=> 'refuse' ,'refuse_reason' => $request->refuse_reason]);

        event(new RefuseAdminPaymentNotify($bank['user'],$bank['orderProject'],$request));
//        $this->notify->NotificationDbType(25,$bank['user'],null,$request->refuse_reason,$bank['orderProject']);

//        $bank->orderProject->delete();
        session()->flash('success','تم رفض الطلب بنجاح');
        return redirect()->back();
    }
}
