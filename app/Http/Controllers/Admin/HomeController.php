<?php

namespace App\Http\Controllers\Admin;


use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Main;


class HomeController extends Controller
{




    public function __construct()
    {


    }

    public function index()
    {

         if (!auth()->check())
            return redirect(route('admin.login'));


        return view('admin.home.index');
    }
}
