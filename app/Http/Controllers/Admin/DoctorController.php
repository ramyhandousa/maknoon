<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DoctorController extends Controller
{

    public function index(Request  $request)
    {
        $query = User::where('defined_user','doctor')
//            ->whereIsActive(1)
                ->select('id','name','phone','city_id','email','is_suspend','created_at');
       if (isset($request->is_accepted)){
            $doctors =   $query->where('is_accepted',0)->get();
            $pageName = 'طلبات الأخصائين الجدد';
            $index = 'new';
       }else{

           $doctors =   $query->where('is_accepted',1)->get();
           $pageName = '  الأخصائين  ';
           $index = 'index';
       }

        return view('admin.doctors.'.$index ,compact('doctors','pageName'));
    }

    public function show($id)
    {
       $user = User::where('defined_user','doctor')->whereIsActive(1)
                    ->with('city','profile','categories','days')->findOrFail($id);

        return view('admin.doctors.show' ,compact('user'));
    }




}
