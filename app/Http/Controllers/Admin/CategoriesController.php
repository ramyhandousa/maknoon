<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Validator;
use UploadImage;


class CategoriesController extends Controller
{

    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/categories/';
    }

    public function index(Request $request)
    {
        $categories = Category::latest()->get();

        $pageName = 'إدارة  التخصصات';

        return view('admin.categories.index',compact('categories','pageName'));
    }
    public function create()
    {
	    $pageName = '   اسم  التخصص';

        return view('admin.categories.create')->with(compact('pageName'));
    }

    public function store(Request $request)
    {
        $category = new Category;
        $category->name_ar = $request->name_ar;
        $category->name_en = $request->name_en;

        if ($request->hasFile('image')):
            $category->image = '/public/' .  $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
        endif;

        if ($category->save()) {

            $url =  route('categories.index');
            $name = ' التخصص  ';

            return response()->json([
                'status' => true,
                "message" => __('trans.addingSuccess',['itemName' => $name]),
                "url" => $url,

            ]);

        }

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $pageName = '         التخصص';

        return view('admin.categories.edit')->with(compact('category', 'pageName' ));
    }


    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->name_ar = $request->name_ar;
        $category->name_en = $request->name_en;

        if ($request->hasFile('image')):
            $category->image =  '/public/' .  $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
        endif;

        if ($category->save()) {

            $url =  route('categories.index');
            $name = '   التخصص';

            return response()->json([
                'status' => true,
                "message" => __('trans.editSuccess',['itemName' => $name]),
                "url" => $url,
            ]);
        }

    }



       public function delete(Request $request){
	    if (!Gate::allows('settings_manage')) {
		 return abort(401);
	    }

	    $model = Category::with('doctors')->findOrFail($request->id);

	    if (count($model['doctors']) > 0) {

		 return response()->json([
		         'status' => false,
		         'message' => "عفواً, لا يمكنك حذف التخصص نظراً لوجود اخصائين  بها"
		 ]);
	    }


	    if ($model->delete()) {
             return response()->json([
                     'status' => true,
                     'data' => $model->id
             ]);
	    }

       }

       public function suspend(Request $request)
       {
	    $model = Category::findOrFail($request->id);
	    $model->is_suspend = $request->type;
	    if ($request->type == 1) {

            $message = "لقد تم حظر التخصص بنجاح";
	    } else {
            $message = "لقد تم فك الحظر على التخصص بنجاح";
	    }
//	    if (count($model->users) > 0) {
//		 return response()->json([
//		         'status' => false,
//		         'message' => 'عفواً, لا يمكنك حظر القسم نظراً لوجود مستخدمين مشتركين فيها'
//		 ]);
//	    }
//
//	    if (count($model->product) > 0) {
//
//		 return response()->json([
//		         'status' => false,
//		         'message' => "عفواً, لا يمكنك حذف القسم نظراً لوجود منتجات  فيها"
//		 ]);
//	    }

	    if ($model->save()) {
		 return response()->json([
		         'status' => true,
		         'message' => $message,
		         'id' => $request->id,
		         'type' => $request->type

		 ]);
	    }

       }



}
