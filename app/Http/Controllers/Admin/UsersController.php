<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\Images;
use App\Http\Requests\Admin\Update_User_vaild;
use App\Libraries\PushNotification;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use UploadImage;
use Validator;

class UsersController extends Controller
{


    public $public_path;
    public $domain;
    public $push;

    public function __construct(PushNotification $push)
    {
        $this->public_path = 'files/';
        $this->domain = URL('/').'/';
        $this->push = $push;
    }

    public function index(Request $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $query = User::where('defined_user','user')->where('is_active', 1);

        $pageName = 'إدارة المستخدمين';

        $users =  $query->latest()->get();

        return view('admin.users.index', compact('users' , 'pageName'));
    }

    public function edit($id){
        $user = User::findOrFail($id);

        return view('admin.users.edit',compact('user'));
    }

    public function create()
    {
        return view('admin.users.add', compact('cities'));
    }

    public function show($id)
    {
        $user = User::where('defined_user','user')->with('children.questions')->findOrFail($id);

        return view('admin.users.show', compact('user'));
    }

    public function accpetedUser(Request $request){

        $user = User::with('devices')->findOrFail($request->id);
        $devices = $user['devices']->pluck('device');
        if ($user){

            $user->update(['is_accepted' => 1]);

            $this->push->sendPushNotification($devices,null,'  الإدارة',  ' تم قبول طلبك من الإدارة '  ,[
                'type' =>  'refresh_token'
            ]);

            return response()->json( [
                'status' => true ,
            ] , 200 );

        }else{

            return response()->json( [
                'status' => false
            ] , 200 );

        }

    }



    public function refuseUser(Request $request){
        $user = User::with('devices')->findOrFail($request->id);
        $devices = $user['devices']->pluck('device');

        $user->update(['is_accepted' => -1,'message' => $request->message]);
        $this->push->sendPushNotification($devices,null,'  الإدارة',  'للأسف.. تم رفض  طلبك من الإدارة '  ,[
            'type' =>  'refresh_token'
        ]);

        session()->flash('success', 'لقد تم  الرفض   بنجاح.');
        return redirect()->back();
    }

    public function update(Update_User_vaild $request, $id){
        $user = User::find($id);
        $user->fill($request->validated());
        if ($request->hasFile('image')):
            $user->image =  $this->public_path   .Images::uploadImage($request,'image',$this->public_path);
        endif;
        $user->save();

        session()->flash('success', 'لقد تم تعديل البيانات  بنجاح.');
        return redirect()->route('users.index');
    }



    public function reAcceptUser(Request $request){
        $user = User::findOrFail($request->id);

        $user->update([  'is_accepted' => 1,'message' => null]);


        return response()->json( [
            'status' => true ,
        ] , 200 );
    }


    public function suspendUser(Request $request)
    {
        $model = User::with('devices','orders','doctor_orders')->findOrFail($request->id);

        $orders = $model['orders']->whereIn('status',['pending','accepted']);
        $doctor_orders = $model['doctor_orders']->whereIn('status',['pending','accepted']);

        if (count($orders) > 0 || count($doctor_orders) > 0 ){
            session()->flash('myErrors',"للأسف ...لا يمكن حظر الأن لان لديه طلبات حالية او جارية");
            return redirect()->back();
        }
        $devices = $model['devices']->pluck('device');

        $model->is_suspend = $request->type;
        $model->message = $request->message;
        if ($request->type == -1) {
            if ($devices){
                collect($model['devices'])->each->delete();
            }
            $this->push->sendPushNotification($devices,null,'حظر الإدارة',  ' تم حظرك من الإدارة بسبب ' . $request->message ,[
                'type' =>  'logout'
            ]);
            $model->is_suspend = 1;
//            $model->api_token = Str::random(60);
            $message = "لقد تم حظر  بنجاح";

        } else {

            $model->message = null;
            $message = "لقد تم فك الحظر بنجاح";
        }

        if ($model->save()) {
            session()->flash('success',$message);
            return redirect()->back();
        }

    }




    public function delete(Request $request)
    {
        $model = User::findOrFail($request->id);

        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }

}
