<?php

namespace App\Http\Controllers\Admin;

use App\Models\BankTransfer;
use App\Models\ContactInvitation;
use App\Models\Invitation;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\OrderProject;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Gate;
class ReportsController extends Controller
{

    public function index(Request $request)
    {
        $pageName = 'تقارير إدارة الطلبات';

        $reports = Order::with('user','category','doctor')->latest()->get();

        return view('admin.Reports.index', compact('reports','pageName'));
    }

    public function show($id ){


        $invitation = Invitation::with('guest_contact')->findOrFail($id);

        $pageName =' المدعوين الخاص بي دعوة '  . $invitation->name ;

        $reports = $invitation['guest_contact'];

        return view('admin.Reports.show', compact('reports','pageName'));

    }

    public function supervisor($id){

        $invitation = Invitation::with('supervisor_contact')->findOrFail($id);

        $pageName =  '  المشرفين الخاص بي دعوة  ' .$invitation->name ;

        $reports = $invitation['guest_contact'];

        return view('admin.Reports.supervisor', compact('reports','pageName'));
    }

}
