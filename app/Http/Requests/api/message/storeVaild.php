<?php

namespace App\Http\Requests\api\message;

use App\Rules\checkOrderChatting;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class storeVaild extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    public function rules()
    {
        return [
            'order_id'          => ['required','exists:orders,id', new checkOrderChatting()],
            'message'           => 'max:2000|required_without_all:url',
            'url'               => 'max:2000|required_without_all:message',
        ];
    }

    public function messages()
    {
        return [
            'message.required_without_all' => 'مطلوب الرسالة من فضلك لانك لم ترفع ملف',
            'url.required_without_all' => 'مطلوب الملف من فضلك'
        ];
    }


    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
