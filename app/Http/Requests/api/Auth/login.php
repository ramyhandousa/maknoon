<?php

namespace App\Http\Requests\api\Auth;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;

class login extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'api_token' => 'required',
//            'password' => 'required',
        ];
    }

   public function messages()
   {
    return [
          'api_token.required' => 'من فضلك firebase id',
          'password.required' => trans('global.required'),
    ];
   }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator){


            Log::info("uuid " . $this->api_token);
//            if (! $token = auth()->attempt(['api_token' => $this->api_token, 'password' => $this->password])) {
            if (! $token = User::whereApiToken($this->api_token)->first() ) {

                $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
                return;
            }

            $user = User::whereApiToken($this->api_token)->first();

            if ($user->is_suspend){
                $validator->errors()->add('is_suspend', trans('global.your_account_suspended') .' '. $user->message);
                return;
            }
        });
    }

      protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
