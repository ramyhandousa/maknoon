<?php

namespace App\Http\Requests\api\Auth;

use App\Rules\checkCodeActivation;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class resendCodeValid extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [

            'email'       => 'required|email|exists:verify_users',

            ];
    }

    public function messages()
    {
        return [
            'email.required' => trans('global.required'),
            'email.email' => trans('validation.email'),
            'email.exists' =>  trans('global.user_not_found'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
