<?php

namespace App\Http\Requests\api\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class validEngineer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'experience_year'   => 'required|numeric|min:0|max:60',
            'cv'                => 'required|exists:upload_images,id',
            'images_work'       => 'required|array',
            'images_work.*'     => 'exists:upload_images,id',
            'desc'              => 'required|string|min:10|max:500',
        ];
    }

    public function messages()
    {
        return [
            'experience_year.required' => 'مطلوب عدد سنين الخبرة من فضلك',
            'cv.required' => 'مطلوب ملف السيرة الذاتية',
            'images_work.required' => 'مطلوب الأعمال السابقة',
            'images_work.array' => 'array   تأكد من ان الإعمال السابقة في شكل  ',
            'desc.required' => 'مطلوب وصف لحضرتك لمساعدة المدير في تلبي طلبك',
        ];
    }


    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $user = Auth::user();
            $profile = $user->profile()->exists();

            if ($profile ) {
                $validator->errors()->add('unavailable', 'لديك طلب إنضمام سابق برجاء الإنتظار .');
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
