<?php

namespace App\Http\Requests\api\Auth;

use App\Models\VerifyUser;
use App\Rules\checkCodeActivation;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\DB;

class checkActivation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'provider'       => 'required',
            'code'        => ['required', 'exists:verify_users,action_code' , new checkCodeActivation()],
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $field = filter_var($this->provider, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

            $user = VerifyUser::where($field ,  $this->provider )->first();

            if (! $user) {
                $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
            }
        });
    }

    public function messages()
    {
        return [
            'email.required' => trans('global.required'),
            'email.email' => trans('validation.email'),
            'email.exists' =>  trans('global.user_not_found'),
            'code.required' => 'إدخل الكود من فضلك',
            'code.exists' =>  trans('global.activation_code_not_correct'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
