<?php

namespace App\Http\Requests\api\Order;

use App\Models\Day;
use App\Models\DoctorDay;
use App\Models\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class updateOrderVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        $order = $this->route('order');
        if ($user->defined_user !== 'user' && $user->id !== $order->user_id){
            return  false;
        }
        return true;
    }

    public function rules()
    {
        $order = Order::findOrFail($this->route('order'));

        $valid = $order->defined_order == "draw";

        return [
            'doctor_id'         => Rule::requiredIf($valid) .'|exists:users,id',
            'child_id'          => Rule::requiredIf($valid) .'|exists:children,id',
            'date_time'         => 'required_if:defined_order,===,appointment|date_format:Y-m-d H:i:s|after:'.date('Y-m-d H:i:s'),
            'price'             => 'required',
            'period'            => 'required_if:defined_order,===,appointment',
            'images'            => Rule::requiredIf($valid) .'|array',
            'images.*'          => Rule::requiredIf($valid) ,
        ];
    }

    public function messages()
    {
        return [
            'images.*.required' => 'مطلوب صور تحليل الرسومات المرفقة ',
            'images.array' => 'array   تأكد من ان الصور المرفقة  في شكل  ',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $user = Auth::user();

            $order = Order::findOrFail($this->route('order'));

            if ($order->defined_order !== 'draw') {

                $dayofweek = date('l', strtotime($this->date_time));

                $day_id = Day::whereNameEn($dayofweek)->first()->id;

                $check_doctor = $this->doctor_id ?: $order->doctor_id;

                $doctor_day = DoctorDay::whereUserIdAndDayId($check_doctor, $day_id);

                $time = Carbon::parse($this->date_time)->format("H:i:s");

                if ($doctor_day->first()){
                    if ($doctor_day->first()->working == 0) {
                        $validator->errors()->add('unavailable', trans('global.user_order_date_unavailable'));
                        return;
                    }

                    if ($doctor_day->first()->working  == 1){
                    $check_time = $doctor_day->where([['start','<=',$time], ['end','>=',$time]])->first() ;

//                        $check_time = $doctor_day->where([ [ 'start','<=',$time], ['end','<=',$time]])->first() ;
                        if (!$check_time){
                            $validator->errors()->add('unavailable',trans('global.user_order_time_unavailable')  );
                            return;
                        }
                    }
                }

            }

            if ($order->defined_order === 'draw') {
                $doctor = User::whereId($order->doctor_id)->whereDefinedUser('doctor')->whereIsAccepted(1)->whereIsSuspend(0);

                if (! $doctor->exists() ) {
                    $validator->errors()->add('unavailable', 'يبدو ان هذا الأخصائي تم حظره او يوجد مشكلة في حسايه .');
                    return;
                }

                if (!$user->whereHas('children')->exists()){ // Check User Have Children
                    $validator->errors()->add('unavailable', trans("global.not_have_children"));
                    return;
                }

                if (!$user->children->contains($this->child_id)){
                    $validator->errors()->add('no_belong', "تأكد من أنك لديك هذا الطفل");
                    return;
                }

                if (! $doctor->whereSubscribeDraw(1)->exists() ) { // Check Doctor Is  Subscribe Draw
                    $validator->errors()->add('unavailable', 'يبدو ان هذا الأخصائي غير مشترك في التحليل بالرسومات .');
                    return;
                }
            }

        });
    }



    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
