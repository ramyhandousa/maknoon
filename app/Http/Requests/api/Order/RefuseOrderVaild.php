<?php

namespace App\Http\Requests\api\Order;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class RefuseOrderVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();
        $order = $this->route('order');

        $vaild = $order->doctor_id == $user->id;

        return [
           'message' => Rule::requiredIf($vaild)
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order = $this->route('order');

            if ( $order->status !== 'pending' ) {
                $validator->errors()->add('order_not_refuse', trans('global.order_not_refuse'));
                return;
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
