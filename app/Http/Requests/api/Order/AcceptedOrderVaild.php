<?php

namespace App\Http\Requests\api\Order;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class AcceptedOrderVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $order = $this->route('order');
        if (Auth::id() == $order->doctor_id){
            return  true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order = $this->route('order');

            if ( $order->status !== 'pending' ) {
                $validator->errors()->add('order_not_refuse', trans('global.order_not_accepted'));
                return;
            }


            if ($order->defined_order != "draw"){
                    $date_now = date("Y-m-d H:i:s");

                    if ($date_now > $order->date_time) {
                        $validator->errors()->add('order_not_refuse', trans('global.order_date_finish'));
                        return;

                    }
            }

        });
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
