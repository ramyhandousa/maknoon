<?php

namespace App\Http\Requests\api\payment;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class payOrderVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment' =>  'required|in:online,bankTransfer',
            'paymentId' => 'required_if:payment,===,online',
            'bankId' => 'required_if:payment,===,bankTransfer|exists:banks,id',
            'image' => 'required_if:payment,===,bankTransfer|exists:upload_images,id',
        ];
    }

    public function messages()
    {
        return [
            'payment.in' => 'bankTransfer او online  يجب ان يكون بين الكلمتين ',
            'paymentId.required_if' => 'من فضلك ادخل الرقم الخاص بعميلة الدفع',
            'bankId.required_if' => 'من فضلك ادخل البنك الخاص بك',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order = $this->route('order');

            $offer = $this->route('orderOffer');

            if ($order->status === 'finish' || $order->status === 'refuse' ){
                $validator->errors()->add('providerId', 'يرجي التأكد من أن هذا الطلب غير مرفوض او غير منتهي لإستكمال الدفع');
                return;
            }

            if ($order->provider_id === null && !$offer){// Make Sure User Accepted Offer
                $validator->errors()->add('providerId', 'يرجي رقم العرض المراد الموافقه عليه');
                return;
            }

            if ($order->provider_id === null && empty($offer->price) ){// Make Sure User Accepted Offer With Price
                $validator->errors()->add('providerOfferPrice', 'يرجي التاكد من أن هذا العرض لديه سعر');
                return;
            }

            if ($order->provider_id !== null && $order->batches_remaining == 0 ) { // Make Sure remain money to transfer
                $validator->errors()->add('unavailable', 'حضرتك لم يعد دفعات متبقية للتحويل');
                return;
            }

            if ($offer){ // Need Offer for first time pay order

                if ($order->id !== $offer->order_id){
                    $validator->errors()->add('offerId', 'هذا العرض لا ينتمي لهذا الطلب');
                    return;
                }

                $bankTransfer = $offer->bank_transfer->where('offer_id',$offer->id)->first();

                if ($this->payment === 'bankTransfer' && $bankTransfer){

                    if ($bankTransfer->is_accepted === 0){ // Valid Transfer Many Times
                        $validator->errors()->add('unavailable', ' برجاء الإنتظار في تحويلك السابق ...  ');
                    }
                }
            }

        });
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
