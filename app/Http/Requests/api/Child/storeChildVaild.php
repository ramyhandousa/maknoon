<?php

namespace App\Http\Requests\api\Child;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class storeChildVaild extends FormRequest
{

    public function authorize()
    {
        $user = Auth::user();
        if ($user->defined_user == "user"){
            return  true;
        }
        return false;
    }


    public function rules()
    {
        return [
            'name'                   => 'required|min:2|max:255',
            'image'                  => 'required|min:10|max:255',
            'age'                    => 'required|min:1|max:100',
            'number_family'          => 'required|min:1|max:100',
            'relationship'           => 'required|in:dad,mother,teacher,other',
            'relationship_other'     => 'required_if:relationship,===,other',
            "questions"              => "required|array",
            "questions.*.question_id"  => "required|distinct|exists:questions,id",
            "questions.*.checked"      => "required|boolean",
        ];
    }


    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
