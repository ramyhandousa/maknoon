<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = request()->headers->get('Accept-Language') ?  : 'ar';
        $body =   trans('global.'.$this->translation) . " " . $this->body ;
        return [
            'id'            => $this->id,
            'title'         => trans('global.'.$this->title),
            'body'          => $body,
            'order_id'      => $this->when($this->order_id,$this->order_id),
//            'type'          => $this->type,
            'is_read'       => (boolean) $this->is_read,
            'time'          => $lang == "ar" ? $this->single_post_arabic_time() :  Carbon::parse($this->created_at)->addHours(2)->format('h:i A'),
            'created_at'    => $this->created_at,

        ];
    }

    function single_post_arabic_time( ) {

        $posttime_h = Carbon::parse($this->created_at)->addHours(2)->format('h');
        $posttime_i = Carbon::parse($this->created_at)->addHours(2)->format('i');
        $posttime_a = Carbon::parse($this->created_at)->addHours(2)->format('A');
        $ampm = array("AM", "PM");
        $ampmreplace = array("صباحا", "مساءً");
        $ar_ampm = str_replace($ampm, $ampmreplace, $posttime_a);

        header('Content-Type: text/html; charset=utf-8');
        $standardletters = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $eastern_arabic_letters = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
        $post_time = $posttime_h . ':' . $posttime_i." ".$ar_ampm;
        return str_replace($standardletters, $eastern_arabic_letters, $post_time);
    }
}
