<?php

namespace App\Http\Resources\Messages;

use App\Http\Resources\User\UserImageResource;
use App\Models\BankTransfer;
use Illuminate\Http\Resources\Json\JsonResource;

class showConversationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $sender = $this->users()->where('id','!=',auth()->id())->first();
        $order = $this->order;
        $offer = $order->offers->first();
        $price = $order->total_price / $offer->batches ;
        return [
            'id'                 => $this->id,
            'order_id'           => $order->id,
            'open'               =>  $this->status == 'open' ,
            'batches_all'        =>  $offer ? $offer->batches : null,
            'batches_remaining'  =>  $order->batches_remaining,
            'price'              =>  $price,
            'has_bank_transfer'  => BankTransfer::whereOrderIdAndIsAccepted($order->id,0)->first() ? true : false,
            'created_at'         =>  $this->getArabicMonth( $this->created_at),
            'user'               =>   new UserImageResource($sender),
            'messages'           =>  $this->paginationLastMessages($request)
        ];
    }



    function paginationLastMessages($request ){


        $last_messages = $this->last_messages->whereNotNull('message')->whereIn('user_id',$this->users()->pluck('id'));

        $pageSize = $request->pageSize;
        $skipCount = $request->skipCount;

        if ($pageSize || $skipCount){

            $currentPage = $request->get('page', 1);

            $data = $last_messages->slice($skipCount + (($currentPage - 1) * $pageSize))->take($pageSize)->values();

        }else{
            $data = $last_messages->take(10);
        }

        return showMessagesResource::collection( $data);
    }

    function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $year = date("Y", strtotime($data));

        $month = $months[$month];

        return $day . ' ' . $month ;
    }
}
