<?php

namespace App\Http\Resources\Messages;

use App\Http\Resources\User\UserFilter;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class showMessagesResource extends ResourceCollection
{
    protected $sender;

    public function __construct($resource, $sender){
        parent::__construct($resource);
        $this->sender = $sender;
        return $this;
    }


    public function toArray($request)
    {
        return $this->collection->map(function ($q) {
            return [
                'id'        =>  $q->id,
                'user_id'   =>  $q->user_id,
                'user'      =>   new UserFilter($this->sender),
                'message'   =>  $this->when($q->message, $q->message ),
                'url'       =>  $this->when($q->url, $q->url ),
                'time'      =>  Carbon::parse($q->created_at)->addHours(2)->format('h:i A') ,
            ];
        });

    }


}
