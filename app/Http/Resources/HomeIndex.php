<?php

namespace App\Http\Resources;

use App\Models\Day;
use App\Models\DoctorDay;
use Illuminate\Http\Resources\Json\JsonResource;

class HomeIndex extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $day_id = Day::whereNameEn(getdate()['weekday'])->first()->id;

        $doctor_day = DoctorDay::whereUserIdAndDayId($this->id, $day_id)->first();
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'image'     => $this->image,
            'rate'      => $this->my_rate()? : "0",
            'working'   =>  (boolean) $doctor_day->working ,
        ];
    }
}
