<?php

namespace App\Http\Resources\Orders;

use App\Http\Resources\Product\ProductIndex;
use App\Http\Resources\User\UserFilter;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class OrderIndex extends JsonResource
{

    public function toArray($request)
    {
        $appointment = $this->defined_order == "appointment" ;

        return [
            'id'                    => $this->id,
            'defined_order'         => $this->defined_order,
            'defined_trans'         => $appointment  ? trans('global.appointment') : trans('global.draw'),
            $this->mergeWhen($this->user_id == Auth::id(),[
                'doctor'            => new UserFilter($this->doctor)
            ]),
            $this->mergeWhen($this->user_id !== Auth::id(),[
                'user'            => new UserFilter($this->user)
            ]),
            'date_time'          => $this->when($this->date_time, date('Y-m-d', strtotime($this->date_time)))
        ];
    }
}
