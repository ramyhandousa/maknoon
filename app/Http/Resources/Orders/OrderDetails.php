<?php

namespace App\Http\Resources\Orders;

use App\Http\Resources\Doctor\DoctorRatingResources;
use App\Http\Resources\User\UserFilter;
use App\Models\Conversation;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class OrderDetails extends JsonResource
{
    public function toArray($request)
    {
        $appointment = $this->defined_order == "appointment" ;
        $lang = request()->headers->get('Accept-Language') ?  : 'ar';

        $conversation = Conversation::whereOrderId($this->id)->first();
        return [
            'id'                    => $this->id,
            'category_id'           => $this->when($this->category_id ,  $this->category_id),
            'category_name'         => $this->when($this->category , $lang == 'ar'? optional($this->category)->name_ar : optional($this->category)->name_en),
            'defined_order'         => $this->defined_order,
            'defined_trans'         => $appointment  ? trans('global.appointment') : trans('global.draw'),
            'date_time'             => $this->date_time,
            'price'                 => $this->price,
            'period'                => $this->period,
            'is_pay'                => (boolean) $this->is_pay,
            'images'                => $this->when($this->images , $this->images),
            $this->mergeWhen($this->status  == "refuse_system",[
                'message'            => trans('global.finish_time_server')
            ]),
            'message'               => $this->when($this->message, $this->message),
            'chatting'                => $conversation ? $conversation->status  == "open" : null,
            'status'                => $this->status,
            'rate'                  =>  $this->when($this->rate,  new DoctorRatingResources($this->rate)),
            $this->mergeWhen($this->child_id ,[
                'child_id'      =>   $this->child_id,
                'child_name'    =>  optional($this->child)->name,
                'report_file'   =>  $this->when($this->report_file , $this->report_file)

            ]),
            $this->mergeWhen($this->user_id == Auth::id(),[
                'doctor'            => new UserFilter($this->doctor)
            ]),
            $this->mergeWhen($this->user_id !== Auth::id(),[
                'user'            => new UserFilter($this->user)
            ]),


        ];
    }
}
