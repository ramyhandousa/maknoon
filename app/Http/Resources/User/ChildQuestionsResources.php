<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class ChildQuestionsResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = request()->headers->get('Accept-Language') ?  : 'ar';
        return [
            'id'        =>  $this->id,
            'name'      =>  $lang == 'ar' ? $this->name_ar  : $this->name_en,
            'checked'   =>  (boolean)  $this->pivot->checked,
        ];
    }
}
