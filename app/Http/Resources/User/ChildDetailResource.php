<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Orders\OrderIndex;
use Illuminate\Http\Resources\Json\JsonResource;

class ChildDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'image'             => $this->when($this->image ,$this->image),
            'age'               => $this->age,
            'number_family'     => $this->number_family,
            'relationship'      => $this->relationship,

            $this->mergeWhen($this->relationship === "other",[
                'message'       => $this->relationship_other
            ]),

            $this->mergeWhen(count($this->questions) > 0 ,[
                'questions' => ChildQuestionsResources::collection($this->questions)
            ]),

            $this->mergeWhen(count($this->orders_finish) > 0 ,[
                'reports' =>  OrderIndex::collection($this->orders_finish)
            ]),

        ];
    }
}
