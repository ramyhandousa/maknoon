<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Lists\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            =>  $this->id,
            'defined_user'  =>  $this->defined_user,
            'name'          =>  $this->name,
            'phone'         =>  $this->phone,
            'email'         =>  $this->email,
            'iso_code'      =>  $this->when($this->iso_code,(string) $this->iso_code),
            'image'         => $this->when($this->image, $this->image),
            'api_token'     =>  $this->api_token,
            'is_active'     =>  (boolean) $this->is_active,
            'city'          => $this->when($this->city, new CityResource($this->city))
        ];
    }
}
