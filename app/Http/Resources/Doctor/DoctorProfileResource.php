<?php

namespace App\Http\Resources\Doctor;

use Illuminate\Http\Resources\Json\JsonResource;

class DoctorProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'degree'            => $this->degree,
            'experience_year'   => $this->experience_year,
            'age'               => $this->age,
            'certificates'      => $this->certificates,
            'desc'              => $this->when($this->desc,$this->desc),
        ];
    }
}
