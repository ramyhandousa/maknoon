<?php

namespace App\Http\Resources\Doctor;

use App\Http\Resources\Lists\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DoctorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                =>  $this->id,
            'defined_user'      =>  $this->defined_user,
            'name'              =>  $this->name,
            'phone'             =>  $this->phone,
            'email'             =>  $this->email,
            'iso_code'          =>  $this->when($this->iso_code,(string) $this->iso_code),
            'image'             =>  $this->when($this->image, $this->image),
            'is_accepted'       =>  (int) $this->is_accepted,
            'is_active'         =>  (boolean) $this->is_active,
            'subscribe_draw'    =>  (boolean) $this->subscribe_draw,
            'api_token'         =>  $this->api_token,
            'rate'              =>  $this->my_rate() ? : "0",
            'city'              => $this->when($this->city, new CityResource($this->city)),

            $this->mergeWhen( $request->has('profile'),[
                'profile' => $this->when($this->profile, new DoctorProfileResource($this->profile))
            ]),

            $this->mergeWhen( $request->query('categories'),[
                'categories' => $this->when($this->categories,  DoctorCategoriesResource::collection($this->categories))
            ]),

            $this->mergeWhen( $request->has('days_working'),[
                'days_working' => $this->when($this->days_working,  DoctorDaysResource::collection($this->days_working))
            ]),

            $this->mergeWhen( $request->has('comments'),[
                'comments' => $this->when($this->ratings,  DoctorRatingResources::collection($this->ratings))
            ]),

        ];
    }
}
