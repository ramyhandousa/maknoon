<?php

namespace App\Http\Resources\Doctor;

use Illuminate\Http\Resources\Json\JsonResource;

class DoctorDaysResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->day_id,
            'working'   => (boolean) $this->working,
            $this->mergeWhen($this->working,[
                'start' => $this->start,
                'end'   => $this->end,
            ])
        ];
    }
}
