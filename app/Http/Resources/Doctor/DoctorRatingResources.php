<?php

namespace App\Http\Resources\Doctor;

use App\Http\Resources\User\UserFilter;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class DoctorRatingResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang=  request()->headers->get('Accept-Language') ;
        return [
            'value'         => $this->value,
            'comment'       => $this->comment,
            'user'          => $this->when($this->user,new UserFilter($this->user)),
            'time'          => $lang == "ar" ? $this->single_post_arabic_time() : Carbon::parse($this->created_at)->addHours(2)->format('h:i A')   ,
            'created_at'    => date('d/m/Y', strtotime( $this->created_at))
        ];
    }


    function single_post_arabic_time( ) {

        $posttime_h =  Carbon::parse($this->created_at)->addHours(2)->format('h');
        $posttime_i = Carbon::parse($this->created_at)->addHours(2)->format('i');
        $posttime_a =  Carbon::parse($this->created_at)->addHours(2)->format('A');
//        $posttime_h = date('h', strtotime( $this->created_at));
//        $posttime_i = date('i', strtotime( $this->created_at));
//        $posttime_a = date('A', strtotime( $this->created_at));
        $ampm = array("AM", "PM");
        $ampmreplace = array("صباحا", "مساءً");
        $ar_ampm = str_replace($ampm, $ampmreplace, $posttime_a);

        header('Content-Type: text/html; charset=utf-8');
        $standardletters = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $eastern_arabic_letters = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
        $post_time = $posttime_h . ':' . $posttime_i." ".$ar_ampm;
        $arabic_time = str_replace($standardletters, $eastern_arabic_letters, $post_time);

        return $arabic_time;
    }
}
