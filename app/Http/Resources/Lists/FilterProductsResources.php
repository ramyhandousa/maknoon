<?php

namespace App\Http\Resources\Lists;

use Illuminate\Http\Resources\Json\JsonResource;

class FilterProductsResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'desc'      => $this->description,
            'price'     => $this->price,
            'quantity'      => $this->quantity,
            'is_offer'  => (boolean) $this->is_offer,
            $this->mergeWhen($this->is_offer == 1,[
                'price_offer' => $this->offer_price
            ]),
            'image'     => $this->when($this->images , $this->images[0]),
            $this->mergeWhen($request->user_id,[

                'is_favorite' => $this->checkInFavorites($request->user_id, $this->id)
            ])
        ];
    }
}
