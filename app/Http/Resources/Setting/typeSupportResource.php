<?php

namespace App\Http\Resources\Setting;

use Illuminate\Http\Resources\Json\JsonResource;

class typeSupportResource extends JsonResource
{

    public function toArray($request)
    {
        $lang = request()->headers->get('Accept-Language') ?  : 'ar';
        return [
            'id' => $this->id,
            'name' =>    $lang == 'ar' ? $this->name_ar  : $this->name_en,
        ];
    }
}
