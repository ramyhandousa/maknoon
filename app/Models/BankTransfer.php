<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BankTransfer extends Model
{

    protected $fillable =[ 'is_accepted','image' ,'message' ] ;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function bank(){
        return $this->belongsTo(Bank::class);
    }

    public function invitation(){

        return $this->belongsTo(Invitation::class,'invitaion_id');
    }

}
