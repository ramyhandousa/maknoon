<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use CanBeScoped;

    protected $guarded = [];

    protected $casts  = [ 'images' => 'array'];


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function doctor(){
        return $this->belongsTo(User::class,'doctor_id');
    }

    public function rate(){
        return $this->hasOne(Rate::class);
    }

    public function child(){
        return $this->belongsTo(Child::class,'child_id');
    }

}
