<?php


namespace App\Models\Collections;


use App\Http\Resources\User\UserFilter;
use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;

class ConversationCustomCollection extends Collection
{

    public function indexData(){
        $lang = request()->headers->get('Accept-Language') ?  : 'ar';

        return  $this->map(function ($conversation) use ($lang){
            $sender = $conversation->users()->where('id','!=',auth()->id())->first();

            $user = $conversation->users()->where('id',auth()->id())->first();

            $badge =  collect($conversation->last_messages)->where('created_at','>', $user->pivot->read_at )->count()   ;

            return [
                'id'          =>  $conversation->id,
                'order_id'    =>  $conversation->order_id,
                'message'     =>   $this->getLastMessage($conversation,$sender),
                'un_read_message'   =>  $badge ,
                'user'        =>    new UserFilter($sender) ,
                'chatting'     => $conversation->status  == "open",
                'status'        =>  Order::find($conversation->order_id) ? Order::find($conversation->order_id)->status : null,
                'created_at'  =>   $lang =='ar' ? $this->getArabicMonth( $conversation->created_at): $conversation->created_at->format('d M'),
            ];
        })->reverse()->values();
    }

    function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $year = date("Y", strtotime($data));

        $month = $months[$month];

        return $day . ' ' . $month ;
    }

    function getLastMessage($conversation , $sender){
        if ( count($conversation->last_messages) > 0 ){

            $message = $conversation->last_messages;

            $data =   collect($message)->values()->first();

            return $data->message ? : $data->url;
        }else{
            return  $sender->name .'  يمكنك بدا المحادثة الأن مع ' ;
        }
    }
}
