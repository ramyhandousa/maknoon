<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    protected $guarded = [];


    public function questions(){
        return $this->belongsToMany(Question::class,'question_user','child_id','question_id')->withPivot('checked');
    }


    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function orders_finish(){
        return $this->hasMany(Order::class)->where('status','=','finish');
    }
}
