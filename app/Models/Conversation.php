<?php

namespace App\Models;

use App\Models\Collections\ConversationCustomCollection;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $guarded = [];

    protected $hidden = array('pivot');

//    public function getCreatedAtAttribute($date)
//    {
////        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d H:i:s');
////        return date('F d ', strtotime($date));
//    }

    public function users(){
        return $this->belongsToMany(User::class)->withPivot('read_at','deleted_at');
    }
    public function messages(){
        return $this->hasMany(Message::class);
    }

    public function last_messages(){
        return $this->hasMany(Message::class)->latest();
    }


    public function order( )
    {
        return $this->belongsTo(Order::class,'order_id');
    }

    public function newCollection(array $models = [])
    {
        return new ConversationCustomCollection($models);
    }


}
