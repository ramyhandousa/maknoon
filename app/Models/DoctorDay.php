<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorDay extends Model
{
    protected $guarded = [];


    public function day(){
        return $this->belongsTo(Day::class);
    }
}
