<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;


class VerifyUser extends Model
{
       protected $fillable = ['user_id','phone' ,'action_code'];


    public function user() {

	    return $this->belongsTo(User::class);
   }

    public static function createOrUpdate($data, $keys)
    {
        $record = self::where($keys)->first();
        if (is_null($record)) {
            return self::create($data);
        } else {
            return self::where($keys)->update($data);
        }
    }
}
