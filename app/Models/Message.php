<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
     protected $fillable = [
        'conversation_id',
        'user_id',
        'date',
        'url',
        'message'
    ];

     protected $dates =[
         'created_at'
     ];



    public function conversations(){
        return $this->belongsTo(Conversation::class,'conversation_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
