<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{


    public function doctors(){
        return $this->belongsToMany(User::class,'category_doctor','user_id','category_id');
    }
}
