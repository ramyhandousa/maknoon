<?php
/**
 * Created by PhpStorm.
 * User: RAMY
 * Date: 4/10/2020
 * Time: 9:29 AM
 */

namespace App\Scoping\InterfaceScope;


use Illuminate\Database\Eloquent\Builder;

interface Scope
{

    public function apply(Builder $builder , $value);

}