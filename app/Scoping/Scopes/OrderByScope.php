<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class OrderByScope implements Scope
{

    public function apply(Builder $builder, $value)
    {

         if($value == 'pending'){

            return $builder->where('is_pay','=',0)->where('status','=','accepted');

        }elseif($value == 'accepted'){

            return $builder->where('is_pay','=',1)->where('status','=','accepted');

        }elseif($value == 'finish'){

            return $builder->where('status','=','finish');

        }elseif($value == 'refuse'){

            return $builder->whereIn('status',['refuse_system','refuse_doctor' ,'refuse_user']);
        }else{

             return $builder->where('status','=','pending');
         }

    }
}
