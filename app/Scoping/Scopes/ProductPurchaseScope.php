<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class ProductPurchaseScope implements Scope
{

    public function apply(Builder $builder, $value)
    {
       return $builder->whereHas('product_purchases',function ($query) use ($value){
          $query->where('purchase_type_id' , $value);
       });
    }
}
