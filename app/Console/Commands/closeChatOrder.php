<?php

namespace App\Console\Commands;

use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Conversation;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class closeChatOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'closeChatOrder:closeChatOrderConversation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'close Chat have order have dateTime period ';

    public $push;
    public  $notify;


    public function __construct(InsertNotification $notification ,PushNotification $push)
    {

        $this->push = $push;
        $this->notify = $notification;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $close_chatting_time = Setting::getBody('close_chatting');

        $now = Carbon::now()->subMinutes($close_chatting_time)->format('Y-m-d H:i');

        $conversations = Conversation::whereHas('order',function ($order) use ($now){
            $order->where('date_time','<=',$now)->where("status",'accepted')->where('is_pay',1);
        })->with('order.user.devices')->where('status','open')->get();



        if (count($conversations) > 0):

            foreach ($conversations as $conversation):
                $conversation->update([ 'status' => 'close'  ]);
                    $notify = $this->notify->NotificationDbType(14,$conversation->order->user_id,null, null,$conversation->order);
                    foreach ($conversation['order']['user']['devices'] as $device):
                        $this->push->sendPushNotification((array) $device['device'], null, $notify['title'], $notify['body'],
                            [
                                'id'            => $notify['id'],
                                'orderId'       => $notify['order_id'],
                                'type'          => $notify['type'],
                                'title'         => $notify['title'],
                                'body'          => $notify['body'],
                                'created_at'    => $notify['created_at'],
                            ]
                        );
                    endforeach;

            endforeach;

        endif;
    }
}
