<?php

namespace App\Console\Commands;

use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Order;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class notifyBeforeOrderComing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifyOrder:notifyBeforeOrderComing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    public $push;
    public  $notify;


    public function __construct(InsertNotification $notification ,PushNotification $push)
    {

        $this->push = $push;
        $this->notify = $notification;
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now()->addHours(2)->format('Y-m-d H:i');

        $event = Order::whereHas('user', function ($user){ // Master Data Of Order Condition
            $user->where('is_active',1)->where('is_suspend',0);
        })->where([
            ['date_time' ,'!=',null],
            ['date_time' ,'<',$now],
            ['time_out','=',0]
        ]);

        $orders  = $event->where(function ($event){
            $event->where("status",'pending')
                ->orWhere("status",'accepted')->where('is_pay',0);
        })->with('user.devices')->get();

        $addMinutest = Setting::getBody('minutes_control');

        if (count($orders) > 0):

            foreach ($orders as $order):
                $orderTime = Carbon::parse($order->date_time)->addHours(2)->subMinutes( $addMinutest  )->format('Y-m-d H:i');

                if(    $orderTime == $now || $now > $orderTime ):
                    $order->update([ 'status' => 'refuse_system' ,'time_out' => 1  ]);
                    $notify = $this->notify->NotificationDbType(8,$order->user_id,null, null,$order);
                    foreach ($order['user']['devices'] as $device):
                        $this->push->sendPushNotification((array) $device['device'], null, $notify['title'], $notify['body'],
                            [
                                'id'            => $notify['id'],
                                'orderId'       => $notify['order_id'],
                                'type'          => $notify['type'],
                                'title'         => $notify['title'],
                                'body'          => $notify['body'],
                                'created_at'    => $notify['created_at'],
                            ]
                        );
                    endforeach;
                endif;

            endforeach;

        endif;
    }


}
