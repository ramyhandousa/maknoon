<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChattingUserNotify
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $conversation;
    public $user;
    public $message;
    public $request;

    public function __construct($conversation,$user,$message,$request)
    {
        $this->conversation         = $conversation;
        $this->user         = $user;
        $this->user         = $user;
        $this->message      = $message;
        $this->request      = $request;
    }

}
