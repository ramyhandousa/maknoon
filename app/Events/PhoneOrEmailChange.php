<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PhoneOrEmailChange
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $user;
    public $code ;
    public $change ;
    public $request ;

    public function __construct( $user , $code , $changeEmailOrPhone , $request)
    {
        $this->user     = $user;
        $this->code     = $code;
        $this->change   = $changeEmailOrPhone;
        $this->request  = $request;
    }


}
