<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ContactUs
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $user;
    public $sender;
    public $request;
    public function __construct($user,$sender,$request)
    {

        $this->user = $user;
        $this->sender = $sender;
        $this->request = $request;
    }


}
