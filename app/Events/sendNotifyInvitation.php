<?php

namespace App\Events;

class sendNotifyInvitation
{

    public $invitation ;
    public $devices ;
    public $status ;

    public function __construct($invitation, $devices ,$status)
    {
        $this->invitation = $invitation;
        $this->devices = $devices;
        $this->status = $status;
    }
}
