<?php

namespace App\Providers;

use App\Models\Notification;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {

            $helper = new \App\Http\Helpers\Images();
            $setting = new Setting();
            $notification_system = Auth::check() ?  Auth::user()->notifications : [];


            $view->with(compact('helper','setting','notification_system'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}


