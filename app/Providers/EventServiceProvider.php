<?php

namespace App\Providers;

use App\Events\ChattingUserNotify;
use App\Events\ContactUs;
use App\Events\NewProjectNotify;
use App\Events\sendNotifyInvitation;
use App\Listeners\ChattingUserNotifyListener;
use App\Listeners\ContactUsListener;
use App\Listeners\NewProjectNotifyListener;
use App\Listeners\sendNotifyInvitationListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ContactUs::class => [
            ContactUsListener::class,
        ],
        sendNotifyInvitation::class => [
            sendNotifyInvitationListener::class
        ],
        NewProjectNotify::class => [
            NewProjectNotifyListener::class
        ],
        ChattingUserNotify::class => [
            ChattingUserNotifyListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
