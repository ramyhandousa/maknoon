<?php

namespace App\Jobs;

use App\Events\sendNotifyInvitation;
use App\Models\Notification;
use App\Models\NotificationInvitation;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendSmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $invitation;
    public $allPhones;
    public $status;
    public function __construct($invitation,$allPhones,$status)
    {
        $this->invitation = $invitation;
        $this->allPhones = $allPhones;
        $this->status = $status ;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        $sms =  json_encode($this->allPhones->implode(',', ','));

        $new_invitation = $this->status == 'store' ? 1 : 0;

        $title_invitation = $new_invitation == 1 ? 'دعوة جديدة ' : 'تعديل دعوة';

//        Sms::sendMessage($title_invitation .$this->invitation->name ,$sms);

        $query = User::whereIn('phone', $this->allPhones);

        $users = $query->get(['id']);

        if ($new_invitation == 1) { // Make Sure New Invitation not Edit invitation

            $notification = Notification::create([
                'user_id'       => $this->invitation->user_id,
                'invitation_id' => $this->invitation->id,
                'title'         => $this->invitation->name,
                'body'          =>   $this->invitation->user->name ." لديك دعوة جديد من  ",
                'type'          => 3
            ]);

            NotificationInvitation::insert($this->notification_invitations($users,$notification));

        }

        $devices = $query->rightJoin('devices','users.id','=','devices.user_id')->get(['device']);

        if (count($devices) > 0){

            $firebase_devices = collect($devices)->pluck('device');

            event(new sendNotifyInvitation($this->invitation,$firebase_devices,$this->status));
        }

    }


    public function notification_invitations($users ,$notification ){
        return collect($users)->map(function ($user) use ($notification){
            return [
                'user_id' => $user['id']  ,
                'notification_id' => $notification->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        })->toArray();
    }
}
