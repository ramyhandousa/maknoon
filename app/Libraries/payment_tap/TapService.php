<?php
/**
 * Created by PhpStorm.
 * User: RAMY
 * Date: 2/24/2020
 * Time: 6:40 PM
 */



namespace App\Libraries\payment_tap;


class TapService
{

    public   function createCharge()
    {
        return new Charge();
    }

    public   function createRefund()
    {
        return new Refund();
    }

    public static function findCharge( $id )
    {
        $charge = new Charge( $id );

        return $charge->find();
    }
}
