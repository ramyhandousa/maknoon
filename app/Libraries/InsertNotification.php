<?php
namespace App\Libraries;

use App\Libraries\PushNotification;
use App\Models\Device;
use  App\Models\Notification;

class InsertNotification {

    public $push;
    function __construct(PushNotification $push)
    {
        $this->push = $push;
    }

    public  function NotificationDbType($type  ,$user,  $sender = null ,  $request = null  , $order = null ,$additional = null){

            switch($type){

            case $type == 1:

                $data = Notification::create([
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'title' =>   trans('global.connect_us')  ,
                    'body' => $request ,
                    'type' => 1,
                ]);

                return $data;

             break;

            case $type == 2:

                // contactUs Form Users
                $data =  Notification::create([
                    'user_id' => 1 ,
                    'sender_id' => $sender ,
                    'title' => trans('global.connect_us'),
                    'body' => $request ,
                    'type' => 2,
                ]);

                return  $data;

                 break;

            case $type == 5:

                $data = Notification::create([
                    'user_id' => $user->id ,
                    'sender_id' => $sender->id,
                    'order_type' => 'product',
                    'order_id' => $order->id,
                    'product_id' => $order->product_id,
                    'title' =>   'طلبات المنتجات'  ,
                    'body' =>  ' تم رفض طلب المنتج من مزود الخدمة ' .   $sender->name .' بسبب  ' . $request  ,
                    'type' => 5,
                ]);

               return  $data;

             break;

            case $type == 6:

                $data = Notification::create([
                    'user_id'       => $user ,
                    'sender_id'     => $sender->id,
                    'order_id'      => $order->id,
                    'title'         =>   'orders'  ,
                    'translation'   => 'new_order',
                    'body'          =>   $sender->name ,
                    'type'          => 6,
                 ]);

                return  $data;
             break;

             case $type == 7:

                $data = Notification::create([
                    'user_id'       => $user ,
                    'sender_id'     => $sender->id,
                    'order_id'      => $order->id,
                    'title'         =>   'orders'  ,
                    'translation'   => 'new_order_draw',
                    'body'          =>   $sender->name ,
                    'type'          => 7,
                ]);

                 return  $data;

              break;

             case $type == 8:

                $data = Notification::create([
                    'user_id'       => $user ,
                    'sender_id'     => 1,
                    'order_id'      => $order->id,
                    'title'         =>  "offers"  ,
                    'translation'   =>    "finish_time_server"  ,
                    "body"             => $order->id ,
                    'type'          => 8,
                ]);
                 return  $data;

             break;

                case $type == 9:

                    $data = Notification::create([
                        'user_id'       => $user ,
                        'sender_id'     => $sender->id,
                        'order_id'      => $order->id,
                        'title'         =>   'orders'  ,
                        'translation'   => 'order_accepted_by',
                        'body'          =>   $sender->name ,
                        'type'          => 9,
                    ]);

                    return  $data;

                    break;
                case $type == 10:

                    $data = Notification::create([
                        'user_id'       => $user ,
                        'sender_id'     => $sender->id,
                        'order_id'      => $order->id,
                        'title'         =>   'orders'  ,
                        'translation'   => 'order_finish_by',
                        'body'          =>   $sender->name ,
                        'type'          => 10,
                    ]);

                    return  $data;

                    break;
                case $type == 11:

                    $data = Notification::create([
                        'user_id'       => $user ,
                        'sender_id'     => $sender->id,
                        'order_id'      => $order->id,
                        'title'         =>   'orders'  ,
                        'translation'   => 'order_payed_by',
                        'body'          =>   $sender->name ,
                        'type'          => 11,
                    ]);
                    return  $data;
                    break;

                case $type == 12:

                    $data = Notification::create([
                        'user_id'       => $user ,
                        'sender_id'     => $sender->id,
                        'order_id'      => $order->id,
                        'title'         =>   'orders'  ,
                        'translation'   => 'order_finish_by',
                        'body'          =>   $sender->name ,
                        'type'          => 12,
                    ]);
                    return  $data;
                    break;

                case $type == 13:

                    $data = Notification::create([
                        'user_id'       => $user ,
                        'sender_id'     => $sender->id,
                        'order_id'      => $order->id,
                        'title'         =>   'orders'  ,
                        'translation'   => 'order_rate_by',
                        'body'          =>   $sender->name ,
                        'type'          => 13,
                    ]);
                    return  $data;
                    break;


                case $type == 14:

                    $data = Notification::create([
                        'user_id'       => $user ,
                        'sender_id'     => 1,
                        'order_id'      => $order->id,
                        'title'         =>  "chatting"  ,
                        'translation'   =>    "finish_time_chatting_server"  ,
                        "body"             => $order->id ,
                        'type'          => 14,
                    ]);
                    return  $data;

                    break;
            default:

     }


    }


    private function insertData($data)
    {
     if (count($data) > 0) {

         $time = ['created_at' => now(),'updated_at' => now()];
           Notification::insert($data   + $time);

     }
    }


}

