<?php
/**
 * Created by PhpStorm.
 * User: RAMY
 * Date: 2/24/2020
 * Time: 6:40 PM
 */

namespace App\Libraries;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class myfatoorah
{

    public $headerApiToken;
    public $token;

    public $basURL;
    public $initiatePayment;
    public $executePayment;

    public $getPaymentStatus;
    public $callBackUrl;
    public $errorUrl;

    public function __construct( )
    {
        $language = request()->headers->get('lang') ? : 'ar';
        app()->setLocale($language);

        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ?   : ' ';

        $this->token = 'pH_Ml_rWC1eEDPiqsRJIpFY1MlhfSsksqEUs3KdJn8odr-HOpsKY-0SqlKS23pYwmjxbILgKZCiFtKmnw98DcAD9q79DXqK41M07mQ__TLiQHzDWD0qinZ07WOv_Z4opkyt6TAgcw6oby2W7ZvLNvCjImTbnPEbnv5xwIXFbEI-gDq1MgNF-wBhtK4sQhfsJYXbmwniEtlZweAjzIJRXwQdtDIoKszlZ0Auob2n0m3HyLwEYJ7c4YdESBW9bWso-AfePRK3dFeeuFEFPEy5sUxOHHOtrdp_3FMQy1yA8LmhAAyzM0E5jl2_AdhFtQBHNwujIVGLc_T3MQLsMZIIEx2xK-X6Gn1ShoTJCSvCmNeB3HB4mLqFhRRTmkW62QHkcti8Vm5Zzhid-MUwa1m1DRpCKSZFz85IISmGspHdRl_CWnqupMDzBNktysG7BMyCRF5SLj5CtPLNlSIzrL0zBdauR0DUfxf0S80DZ-GsW23eh01trVCAvBt-IYtdgHta-tawjWbXTcVBMNXlYDDPoQJ9Tia1L5dlwEDQ-rnwdm8WQM5ouwn4wzT7DxSXshV1NqVPEkC4zMRL1_sPuN0YMBEKjzUDOxB6AjSrPQS3TELllCA1pCHsQWxqePH7yEnMGfZlJj9rv0Gn84uRekMUl4a3HZiYs0tcj7aonvXU4UEq7pptVh7ryHMpTUTWh0WM8euBElISx7pXONXVPs3UHWUIYyrM';
//        $this->token = '7Fs7eBv21F5xAocdPvvJ-sCqEyNHq4cygJrQUFvFiWEexBUPs4AkeLQxH4pzsUrY3Rays7GVA6SojFCz2DMLXSJVqk8NG-plK-cZJetwWjgwLPub_9tQQohWLgJ0q2invJ5C5Imt2ket_-JAlBYLLcnqp_WmOfZkBEWuURsBVirpNQecvpedgeCx4VaFae4qWDI_uKRV1829KCBEH84u6LYUxh8W_BYqkzXJYt99OlHTXHegd91PLT-tawBwuIly46nwbAs5Nt7HFOozxkyPp8BW9URlQW1fE4R_40BXzEuVkzK3WAOdpR92IkV94K_rDZCPltGSvWXtqJbnCpUB6iUIn1V-Ki15FAwh_nsfSmt_NQZ3rQuvyQ9B3yLCQ1ZO_MGSYDYVO26dyXbElspKxQwuNRot9hi3FIbXylV3iN40-nCPH4YQzKjo5p_fuaKhvRh7H8oFjRXtPtLQQUIDxk-jMbOp7gXIsdz02DrCfQIihT4evZuWA6YShl6g8fnAqCy8qRBf_eLDnA9w-nBh4Bq53b1kdhnExz0CMyUjQ43UO3uhMkBomJTXbmfAAHP8dZZao6W8a34OktNQmPTbOHXrtxf6DS-oKOu3l79uX_ihbL8ELT40VjIW3MJeZ_-auCPOjpE3Ax4dzUkSDLCljitmzMagH2X8jN8-AYLl46KcfkBV';

        $this->basURL           = 'https://api.myfatoorah.com';
//        $this->basURL           = 'https://apitest.myfatoorah.com';
        $this->initiatePayment  = $this->basURL . '/v2/InitiatePayment';
        $this->executePayment   = $this->basURL . '/v2/ExecutePayment';
        $this->getPaymentStatus  = $this->basURL . '/v2/GetPaymentStatus';
        $this->callBackUrl      = request()->root() . '/payments/success?orderId=' . \request()->orderId;
        $this->errorUrl         =  request()->root() . '/payments/errors?orderId=' . \request()->orderId;
    }


    public function listPaymentMethod(Request $request){

//        $user = Auth::user();
        $user = User::where('api_token',$this->headerApiToken)->first();

        $fileds = $this->fields($user,$request,$this->initiatePayment);

        $response = $this->myfatoorahResponse($this->initiatePayment,$fileds);

        return $response;
    }

    public function excutePaymentMethod(Request $request){

//        $user = Auth::user();
        $user = User::where('api_token',$this->headerApiToken)->first();
        $fileds = $this->fields($user,$request,$this->executePayment);

         $response = $this->myfatoorahResponse($this->executePayment,$fileds);

        return $response;
    }
    public function getStatus(Request $request){

        $user = User::where('api_token',$this->headerApiToken)->first();

        $fileds = $this->fields($user,$request,$this->getPaymentStatus);

        $response = $this->myfatoorahResponse($this->getPaymentStatus,$fileds);

        return $response;
    }
    public function myfatoorahResponse($curlUrl , $fields ){

         $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $curlUrl,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $fields,
            CURLOPT_HTTPHEADER => array("Authorization: Bearer $this->token", "Content-Type: application/json"),

        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $results =  json_decode($response, true);

        return $results;
    }

    public function fields($user , $request , $curlUrl){

        if ($curlUrl === $this->executePayment) {

            $fields = '{
              "PaymentMethodId": ' . $request->paymentMethodId . ',
              "orderId": ' . $request->orderId . ',
              "CustomerName": "' . $user->name . '",
              "MobileCountryCode": "+966",
              "CustomerMobile": "' . $user->phone . '",
//              "CustomerEmail": "' . $user->email . '",
              "InvoiceValue": ' . $request->total . ',
              "CallBackUrl": "' . $this->callBackUrl . '",
              "ErrorUrl": "' . $this->errorUrl . '",
              "Language": "en",
              "CustomerAddress": {
                "Block": "",
                "Street": "",
                "HouseBuildingNo": "",
                "Address": "' . $user->address . '",
                "AddressInstructions": ""
              },

              "SupplierCode": 0,
              "InvoiceItems": [
                {
                  "ItemName": "Service",
                  "Quantity": 1,
                  "UnitPrice": ' . $request->total . '
                }
              ]
            }';

        }elseif($curlUrl === $this->getPaymentStatus){

            $fields = "{  \"keyType\":  \"PaymentId\"  ,\"key\": \"$request->paymentId\"}";

        }else{

            $fields = "{\"InvoiceAmount\": $request->total  ,\"CurrencyIso\": \"SAR\"}";

        }

        return $fields;
    }

    public function success(Request $request){

        return response()->json([
            'status' => 200,
            'message'=> "Success Payment.",
            'data' => $request->all()
        ], 200);
    }

    public function errors(Request $request){

        return response()->json([
            'status' => 400,
            'message'=> "Error Payment.",
            'error' =>  (array)$request->all()
        ], 400);
    }

}
